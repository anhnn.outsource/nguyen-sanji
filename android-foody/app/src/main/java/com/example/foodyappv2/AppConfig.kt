package com.example.foodyappv2

object AppConfig {
    const val PAGE_LIMIT_DEFAULT = 20
    const val PAGE_LIMIT_LARGE = 50
    const val DEEPLINK_PREFIX = "foodyapp://"
}
