package com.example.foodyappv2.common.imageloader


internal object ImageLoaderFactory {

    fun glide(): IImageLoader {
        return GlideImageLoader
    }

}