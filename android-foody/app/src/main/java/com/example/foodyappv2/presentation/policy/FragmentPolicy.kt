package com.example.foodyappv2.presentation.policy

import com.example.foodyappv2.R
import com.example.foodyappv2.base.BaseFragment
import com.example.foodyappv2.databinding.FragmentRuleBinding
import com.example.foodyappv2.utils.onDebouncedClick

class FragmentPolicy : BaseFragment<FragmentRuleBinding>() {

    override fun isCanBackPress() = true

    override fun getLayoutId() = R.layout.fragment_rule

    override fun initView() {

    }

    override fun setAction() {
        binding.ivBack.onDebouncedClick {
            onBackPress()
        }
    }
}