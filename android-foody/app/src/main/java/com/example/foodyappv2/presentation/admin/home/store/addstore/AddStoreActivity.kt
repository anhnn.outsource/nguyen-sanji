package com.example.foodyappv2.presentation.admin.home.store.addstore

import ai.ftech.dev.base.common.ActivityConfig
import ai.ftech.dev.base.common.BaseActivity
import ai.ftech.dev.base.common.bindingadapter.onSafeClick
import android.content.ActivityNotFoundException
import android.content.Intent
import android.graphics.drawable.BitmapDrawable
import android.net.Uri
import android.provider.MediaStore
import android.util.Log
import android.widget.Toast
import com.example.foodyappv2.R
import com.example.foodyappv2.base.extension.getAppString
import com.example.foodyappv2.data.event.AddFoodEvent
import com.example.foodyappv2.data.event.AddStoreEvent
import com.example.foodyappv2.databinding.AddStoreActivityBinding
import com.google.android.gms.tasks.OnSuccessListener
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import org.greenrobot.eventbus.EventBus
import java.io.IOException
import kotlin.random.Random

class AddStoreActivity : BaseActivity<AddStoreActivityBinding>() {

    private val REQUEST_IMAGE_CODE = 1
    private lateinit var filePath: Uri
    private lateinit var storage: StorageReference
    private lateinit var firebaseStorage: FirebaseStorage
    private val db = FirebaseFirestore.getInstance()
    private val imageNumber =
        generateSequence { Random.nextInt(1000) }.distinct().take(4).toSet()
    private val imageUrl = "image${imageNumber}.jpg"
    override fun getConfig(): ActivityConfig {
        return object : ActivityConfig {
            override fun getLayoutResource(): Int {
                return R.layout.add_store_activity
            }
        }
    }

    override fun onInitView() {
        // tạo db
        firebaseStorage = FirebaseStorage.getInstance()
        storage = firebaseStorage.reference

        with(binding) {
            ivBack.onSafeClick {
                finish()
            }

            // tạo cửa hàng
            tvComplete.onSafeClick {

                // check dữ liệu
                if (checkInput()) {

                    addStore()

                    // bắn thông báo
                    EventBus.getDefault().post(AddStoreEvent())

                    // back lại màn trước
                    finish()
                } else {

                    // thông báo thất bại
                    Toast.makeText(
                        this@AddStoreActivity,
                        getAppString(R.string.failure),
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }

            // mở file ảnh
            cvStoreImage.onSafeClick {
                dispatchTakePictureIntent()
            }
        }
    }

    // 1 - check dữ liệu vào
    private fun checkInput(): Boolean {
        return (binding.edtName.text.toString() != ""
                && binding.edtAddress.text.toString() != ""
                && binding.edtCategory.text.toString() != ""
                && binding.edtCategory.text.toString() != "")
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_IMAGE_CODE && resultCode == RESULT_OK) {
            if (!(data != null && data.data != null)) {
                return
            }
            filePath = data.data!!

            try {
                val bitmap = MediaStore.Images.Media.getBitmap(contentResolver, filePath)
                val ob = BitmapDrawable(resources, bitmap)
                binding.ivStoreImage.setImageDrawable(ob)
                uploadImage()

            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
    }

    // 2  - thêm cửa hàng
    private fun addStore() {
        try {
            val food = hashMapOf(
                "name" to binding.edtName.text.toString(),
                "address" to binding.edtAddress.text.toString(),
                "category" to binding.edtCategory.text.toString(),
                "rate" to binding.edtRate.text.toString()
            )
            db.collection("stores")
                .add(food)
                .addOnSuccessListener { documentReference ->

                    db.collection("stores").document(documentReference.id)
                        .update("id", documentReference.id)
                        .addOnSuccessListener {

                        }.addOnFailureListener {

                        }

                    val ref = imageUrl.let {
                        storage.child(it)
                    }
                    ref.downloadUrl
                        .addOnSuccessListener(OnSuccessListener<Uri?> {

                            db.collection("stores").document(documentReference.id)
                                .update("image", it).addOnSuccessListener {
                                    EventBus.getDefault().post(AddStoreEvent())
                                    finish()
                                }
                        })
                }.addOnFailureListener { e ->
                    Toast.makeText(this, e.message, Toast.LENGTH_SHORT).show()
                }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
    private fun dispatchTakePictureIntent() {
        val intent = Intent()
        intent.type = "image/*"
        intent.action = Intent.ACTION_GET_CONTENT
        try {
            startActivityForResult(
                Intent.createChooser(intent, "Select Picture"),
                REQUEST_IMAGE_CODE
            )
        } catch (e: ActivityNotFoundException) {

        }
    }

    private fun uploadImage() {
        val ref =
            storage.child(imageUrl)
        ref.putFile(filePath)
    }
}