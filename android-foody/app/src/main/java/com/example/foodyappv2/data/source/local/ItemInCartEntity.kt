package com.example.foodyappv2.data.source.local

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "tbl_itemincart")
class ItemInCartEntity(
    @PrimaryKey(autoGenerate = true)
    val id:Int=0,
    @ColumnInfo(name = "idFood")
    val idFood:String,
    @ColumnInfo(name = "nameItem")
    val nameItem:String,
    @ColumnInfo(name = "amount")
    val amount:Int,
    @ColumnInfo(name = "total")
    val total:Int
) {

}