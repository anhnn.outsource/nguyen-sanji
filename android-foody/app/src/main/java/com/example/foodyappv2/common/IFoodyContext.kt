package com.example.foodyappv2.common

import com.example.foodyappv2.base.extension.getAppString
import android.content.Context
import com.example.foodyappv2.R
import com.example.foodyappv2.common.widget.headeralert.HEADER_ALERT_TIME_SHOWN
import com.example.foodyappv2.common.widget.headeralert.HEADER_ALERT_TYPE

interface IFoodyContext {
    fun getActivityContext(): Context
    fun showLoading(message: String = getAppString(R.string.loading))
    fun hideLoading()
    fun showError(message: String?)
    fun showHeaderAlert(
        msg: String?,
        type: HEADER_ALERT_TYPE,
        timeShown: HEADER_ALERT_TIME_SHOWN = HEADER_ALERT_TIME_SHOWN.DELAY_DEFAULT
    )
}
