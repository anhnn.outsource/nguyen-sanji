package com.example.foodyappv2.presentation.admin.home.ordered

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.foodyappv2.AppPreference
import com.example.foodyappv2.data.model.ItemInCart
import com.example.foodyappv2.data.model.Ordered
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query

class AdminOrderViewModel : ViewModel() {

    var firestore: FirebaseFirestore = FirebaseFirestore.getInstance()

    var ordereds = MutableLiveData<List<Ordered>>()

    var successOrders = MutableLiveData<List<Ordered>>()
    var successTotal = MutableLiveData<Long>(0)

    var processingOrders = MutableLiveData<List<Ordered>>()
    var processingTotal = MutableLiveData<Long>(0)

    var canceledOrders = MutableLiveData<List<Ordered>>()
    var canceledTotal = MutableLiveData<Long>(0)

    fun getData() {
        val productRefs =
            firestore.collection("ordered").orderBy("date", Query.Direction.DESCENDING)
        val results = mutableListOf<Ordered>()
        val successResults = mutableListOf<Ordered>()
        val processingResults = mutableListOf<Ordered>()
        val canceledResults = mutableListOf<Ordered>()
        productRefs
            .get().addOnSuccessListener {
                it?.let { it ->
                    it.forEach {
                        val order = Ordered().apply {
                            id = it.get("id").toString()
                            name = it.get("name").toString()
                            total = it.get("total") as Long
                            paymentMethod = it.get("paymentMethod").toString()
                            amount = it.get("amount") as Long
                            date = it.get("date").toString()
                            status = it.get("status") as Long
                            discount = it.get("discount") as Long

                            var items: MutableList<HashMap<String, String>> =
                                it.get("itemInCarts") as MutableList<HashMap<String, String>>
                            var itemInCarts = mutableListOf<ItemInCart>()
                            items.forEach {
                                var itemInCart = ItemInCart().apply {
                                    this.id = it.get("id").toString()
                                    this.nameItem = it.get("nameItem").toString()
                                    this.amount = it.get("amount") as Long
                                    this.total = it.get("total") as Long
                                }
                                itemInCarts.add(itemInCart)
                            }
                            this.itemInCarts = itemInCarts

                        }
                        results.add(order)
                        when (order.status) {
                            0L -> {
                                processingResults.add(order)
                                processingTotal.value = processingTotal.value?.plus(order.total ?: 0)
                            }
                            1L -> {
                                successResults.add(order)
                                successTotal.value = successTotal.value?.plus(order.total ?: 0)
                            }
                            2L -> {
                                canceledResults.add(order)
                                canceledTotal.value = canceledTotal.value?.plus(order.total ?: 0)
                            }
                        }
                    }
                    ordereds.postValue(results)
                    successOrders.postValue(successResults)
                    processingOrders.postValue(processingResults)
                    canceledOrders.postValue(canceledResults)
                }
            }.addOnFailureListener {

            }
    }

}