package com.example.foodyappv2.base.adapter

import android.view.View
import androidx.recyclerview.widget.RecyclerView

internal open class BaseViewHolder<DATA>(itemView : View) : RecyclerView.ViewHolder(itemView){

    open fun onBind(vhData: DATA) {

    }

    open fun onBind(vhData: DATA, payloads: MutableList<Any>) {

    }
}
