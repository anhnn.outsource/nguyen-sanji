package com.example.foodyappv2.presentation.test

import ai.ftech.dev.base.parcelable.IParcelable
import android.net.Uri
import android.os.Parcel
import android.os.Parcelable

class ImageModel() : IParcelable {
    var id: Long? = -1L
    var displayName: String? = ""
    var dateAdded: Long? = -1L
    var contentUri: Uri? = null

    constructor(parcel: Parcel) : this() {
        id = parcel.readLong()
        displayName = parcel.readString().toString()
        dateAdded = parcel.readLong()
        contentUri = parcel.readParcelable(Uri::class.java.classLoader)
    }

    override fun writeToParcel(dest: Parcel?, flags: Int) {
        dest?.writeLong(id!!)
        dest?.writeString(displayName)
        dest?.writeLong(dateAdded!!)
        dest?.writeParcelable(contentUri, flags)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ImageModel> {
        override fun createFromParcel(parcel: Parcel): ImageModel {
            return ImageModel(parcel)
        }

        override fun newArray(size: Int): Array<ImageModel?> {
            return arrayOfNulls(size)
        }
    }
}