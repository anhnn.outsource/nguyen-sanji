package com.example.foodyappv2.data.modelserver

import com.example.foodyappv2.data.model.Food
import com.google.gson.annotations.SerializedName

data class Content(
    @SerializedName("datalist")
    val datalist: MutableList<Food>
)