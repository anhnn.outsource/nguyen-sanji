package com.example.foodyappv2.common

import ai.ftech.dev.base.common.BaseActivity
import com.example.foodyappv2.base.extension.observer
import com.example.foodyappv2.common.widget.loading.LoadingDialog
import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.app.AppCompatDelegate
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.LiveData
import com.example.foodyappv2.common.action.AppActionCallback
import com.example.foodyappv2.common.action.AppActionResult
import com.example.foodyappv2.common.widget.headeralert.HEADER_ALERT_TIME_SHOWN
import com.example.foodyappv2.common.widget.headeralert.HEADER_ALERT_TYPE
import com.example.foodyappv2.common.widget.headeralert.HeaderAlertDefault

abstract class FoodyActivity<DB : ViewDataBinding> :
    BaseActivity<DB>(),
    IFoodyContext {

    private var headerAlert: HeaderAlertDefault? = null
    private var loadingDialog: LoadingDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onResume() {
        super.onResume()
    }

    override fun onPause() {
        super.onPause()
    }

    override fun onDestroy() {
        loadingDialog = null
        super.onDestroy()
    }

    override fun onPrepareInitView() {
        super.onPrepareInitView()
        setStatusColor(
            color = if (AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES) {
                Color.TRANSPARENT
            } else {
                Color.BLACK
            },
            isDarkText = (AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES)
        )
        loadingDialog = LoadingDialog()
    }

    override fun onInitView() {
    }

    override fun onObserverViewModel() {
        super.onObserverViewModel()
    }

    override fun onViewClick() {
        super.onViewClick()
    }

    override fun getActivityContext(): Context = this


    override fun showLoading(message: String) {
        loadingDialog?.showDialog(supportFragmentManager, LoadingDialog::class.java.simpleName)
    }

    override fun hideLoading() {
        loadingDialog?.dismissDialog()
    }

    override fun showError(message: String?) {
//        showHeaderAlert(message, HEADER_ALERT_TYPE.ERROR, HEADER_ALERT_TIME_SHOWN.DELAY_2_SECOND)
    }

    override fun showHeaderAlert(
        msg: String?,
        type: HEADER_ALERT_TYPE,
        timeShown: HEADER_ALERT_TIME_SHOWN
    ) {
        if (headerAlert == null) {
            headerAlert = HeaderAlertDefault(this).apply {
                this.timeShown = timeShown
            }
        }
        if (type == HEADER_ALERT_TYPE.ERROR && TextUtils.isEmpty(msg)) {
//            msg = resources.getString(R.string.common_error_header)
        }
        headerAlert?.show(msg, type)
    }

    protected fun <T> observerViewModelAction(
        liveAction: LiveData<AppActionResult<T>>,
        callback: AppActionCallback<T>
    ) {
        observer(liveAction) {
            if (it?.resultStatus == AppActionResult.RESULT_STATUS.ERROR) {
                callback.onException(it.exception)
                return@observer
            } else if (it?.resultStatus == AppActionResult.RESULT_STATUS.SUCCESS) {
                callback.onSuccess(it.data)
                return@observer
            }
        }
    }

    fun hideKeyBoard(v: View) {
        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(v.windowToken, 0)
    }

    fun showKeyBoard(v: View) {
        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.showSoftInput(v, InputMethodManager.SHOW_IMPLICIT)
    }
}
