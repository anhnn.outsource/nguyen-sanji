package com.example.foodyappv2.presentation.storedetail


import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.foodyappv2.R
import com.example.foodyappv2.databinding.ItemStoreImageBinding


class StoreImageAdapter : RecyclerView.Adapter<StoreImageAdapter.FoodImageViewHolder>() {
    var list: MutableList<String> = mutableListOf()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    var callBack: FoodImageListener? = null

    override fun getItemCount() = list.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FoodImageViewHolder {

        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = DataBindingUtil.inflate<ItemStoreImageBinding>(
            layoutInflater,
            R.layout.item_store_image,
            parent,
            false
        )
        return FoodImageViewHolder(binding)
    }

    override fun onBindViewHolder(holder: FoodImageViewHolder, position: Int) {
        val item = list[position]
        holder.binding.item = item
        holder.binding.itemPosition = position
        holder.binding.itemListener = callBack
    }

    class FoodImageViewHolder(val binding: ItemStoreImageBinding) :
        RecyclerView.ViewHolder(binding.root) {

    }

    interface FoodImageListener {
        fun onItemClick(index: Int, item: String)
    }

}