package com.example.foodyappv2.data.model

import androidx.room.ColumnInfo
import androidx.room.PrimaryKey
import com.example.foodyappv2.data.source.local.ItemInCartEntity

class Ordered {
    var id: String? = null
    var name: String? = null
    var total: Long? = null
    var paymentMethod: String? = "Tiền mặt"
    var amount: Long? = null
    var date: String? = null
    var status: Long? = null
    var itemInCarts: MutableList<ItemInCart>? = null
    var discount: Long? = 0


    fun getStatus(): String {
        return when (status) {
            0L -> "Đang giao hàng"
            1L -> "Đã hoàn thành"
            else -> "Đã hủy"
        }
    }
}