package com.example.foodyappv2.presentation.description

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.example.foodyappv2.R
import com.example.foodyappv2.databinding.BottomSheetRatingBinding

import com.google.android.material.bottomsheet.BottomSheetDialogFragment

class RatingBottomSheet: BottomSheetDialogFragment() {

    lateinit var binding: BottomSheetRatingBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.bottom_sheet_rating, container, false)
        return binding.root
    }


}