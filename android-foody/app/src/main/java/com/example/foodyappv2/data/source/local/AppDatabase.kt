package com.example.foodyappv2.data.source.local

import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.foodyappv2.utils.getApplication


@Database(entities = [
    SearchEntity::class,
    ItemInCartEntity::class,
    OrderEntity::class],
    version = 4)
abstract class AppDatabase : RoomDatabase() {

    abstract fun searchDao(): SearchDao
    abstract fun itemInCartDao(): ItemInCartDao
    abstract fun orderDao(): OrderDao

    companion object {
        fun getInstance(): AppDatabase {
            val db = Room.databaseBuilder(
                getApplication(),
                AppDatabase::class.java,
                "food.db"
            )
                .allowMainThreadQueries()
                .addMigrations()
                .fallbackToDestructiveMigration()
                .build()

            return db
        }
    }
}

