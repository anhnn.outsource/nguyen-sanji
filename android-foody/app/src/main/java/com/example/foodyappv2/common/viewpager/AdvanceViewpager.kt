package com.example.foodyappv2.common.viewpager

import android.content.Context
import android.os.Handler
import android.os.Looper
import android.util.AttributeSet
import androidx.viewpager.widget.ViewPager
import java.util.*

class AdvanceViewpager @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null
) : ViewPager(context, attrs) {

    var DELAY = 500L
    var PERIOD = 5000L
    var isRepeatSwipe = true
        set(value) {
            field = value
            if (isAttachedToWindow) {
                handleAutoSlide()
            }
        }

    private var repeatTime: Timer? = null
    private val repeatHandler = Handler(Looper.getMainLooper())
    private val repeatRunnable = Runnable {
        var currentPage = currentItem
        currentPage++
        if (currentPage == adapter?.count) {
            currentPage = 0
        }
        setCurrentItem(currentPage, true)
    }

    init {

    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        repeatTime = Timer()
        handleAutoSlide()
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        repeatHandler.removeCallbacksAndMessages(null)
        removeInstanceTimer()
    }

    private fun handleAutoSlide() {
        if (isRepeatSwipe) {
            repeatTime?.schedule(object : TimerTask() {
                override fun run() {
                    repeatHandler.post(repeatRunnable)
                }
            }, DELAY, PERIOD)
        } else {
            repeatHandler.removeCallbacksAndMessages(null)
            removeInstanceTimer()
        }
    }

    private fun removeInstanceTimer() {
        repeatTime?.cancel()
        repeatTime?.purge()
        repeatTime = null
    }
}
