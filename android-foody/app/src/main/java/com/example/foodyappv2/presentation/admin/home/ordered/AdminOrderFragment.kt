package com.example.foodyappv2.presentation.admin.home.ordered

import android.annotation.SuppressLint
import android.view.View
import androidx.fragment.app.viewModels
import androidx.transition.Visibility
import com.example.foodyappv2.R
import com.example.foodyappv2.base.BaseFragment
import com.example.foodyappv2.base.extension.getAppDrawable
import com.example.foodyappv2.base.extension.gone
import com.example.foodyappv2.base.extension.show
import com.example.foodyappv2.databinding.AdminOrderFragmentBinding
import com.example.foodyappv2.utils.observer
import com.example.foodyappv2.utils.onDebouncedClick
import com.google.firebase.firestore.FirebaseFirestore
import org.koin.android.ext.android.bind

class AdminOrderFragment : BaseFragment<AdminOrderFragmentBinding>() {
    var firestore: FirebaseFirestore = FirebaseFirestore.getInstance()
    private val viewModel by viewModels<AdminOrderViewModel>()

    private var adapter = AdminOrderAdapter()

    override fun getLayoutId(): Int {
        return R.layout.admin_order_fragment
    }

    @SuppressLint("SetTextI18n")
    override fun initView() {
        viewModel.getData()

        binding.rvAdminOrderSuccess.adapter = adapter
        binding.rvAdminOrderProcessing.adapter = adapter
        binding.rvAdminOrderCanceled.adapter = adapter

        observer(viewModel.successOrders) {
            binding.tvAdminOrderOrderSuccessCount.text =
                "Số lượng:  ${viewModel.successOrders.value?.size.toString()}"
        }

        observer(viewModel.processingOrders) {
            binding.tvAdminOrderOrderProcessingCount.text =
                "Số lượng:  ${viewModel.processingOrders.value?.size.toString()}"
        }

        observer(viewModel.canceledOrders) {
            binding.tvAdminOrderOrderCanceledCount.text =
                "Số lượng:  ${viewModel.canceledOrders.value?.size.toString()}"
        }

        observer(viewModel.successTotal) {
            binding.tvAdminOrderOrderSuccessTotal.text =
                "Doanh thu:  ${viewModel.successTotal.value.toString()}"
        }

        observer(viewModel.processingTotal) {
            binding.tvAdminOrderOrderProcessingTotal.text =
                "Doanh thu:  ${viewModel.processingTotal.value.toString()}"
        }

        observer(viewModel.canceledTotal) {
            binding.tvAdminOrderOrderCanceledTotal.text =
                "Doanh thu:  ${viewModel.canceledTotal.value.toString()}"
        }
    }

    override fun setAction() {
        with(binding) {
            tvAdminOrderOrderSuccessMore.onDebouncedClick {
                if (rvAdminOrderSuccess.visibility == View.VISIBLE) {
                    tvAdminOrderOrderSuccessMore.setImageResource(R.drawable.right)
                    rvAdminOrderSuccess.gone()
                } else {
                    rvAdminOrderSuccess.show()
                    tvAdminOrderOrderSuccessMore.setImageResource(R.drawable.down)
                    viewModel.successOrders.value?.let { it1 -> adapter.setData(it1) }
                }
                rvAdminOrderCanceled.gone()
                rvAdminOrderProcessing.gone()
                tvAdminOrderOrderCanceledMore.setImageResource(R.drawable.right)
                tvAdminOrderOrderProcessingMore.setImageResource(R.drawable.right)
            }

            tvAdminOrderOrderCanceledMore.onDebouncedClick {
                if (rvAdminOrderCanceled.visibility == View.VISIBLE) {
                    rvAdminOrderCanceled.gone()
                    tvAdminOrderOrderCanceledMore.setImageResource(R.drawable.right)
                } else {
                    tvAdminOrderOrderCanceledMore.setImageResource(R.drawable.down)
                    rvAdminOrderCanceled.show()
                    viewModel.canceledOrders.value?.let { it1 -> adapter.setData(it1) }
                }
                rvAdminOrderSuccess.gone()
                rvAdminOrderProcessing.gone()
                tvAdminOrderOrderSuccessMore.setImageResource(R.drawable.right)
                tvAdminOrderOrderProcessingMore.setImageResource(R.drawable.right)
            }

            tvAdminOrderOrderProcessingMore.onDebouncedClick {
                if (rvAdminOrderProcessing.visibility == View.VISIBLE) {
                    rvAdminOrderProcessing.gone()
                    tvAdminOrderOrderProcessingMore.setImageResource(R.drawable.right)
                } else {
                    tvAdminOrderOrderProcessingMore.setImageResource(R.drawable.down)
                    rvAdminOrderProcessing.show()
                    viewModel.processingOrders.value?.let { it1 -> adapter.setData(it1) }
                }
                rvAdminOrderSuccess.gone()
                rvAdminOrderCanceled.gone()
                tvAdminOrderOrderCanceledMore.setImageResource(R.drawable.right)
                tvAdminOrderOrderSuccessMore.setImageResource(R.drawable.right)
            }
        }
    }
}
