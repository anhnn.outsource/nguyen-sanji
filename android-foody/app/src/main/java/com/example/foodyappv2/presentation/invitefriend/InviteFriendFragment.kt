package com.example.foodyappv2.presentation.invitefriend

import com.example.foodyappv2.R
import com.example.foodyappv2.base.BaseFragment
import com.example.foodyappv2.databinding.FragmentInviteFriendBinding
import com.example.foodyappv2.utils.onDebouncedClick

class InviteFriendFragment : BaseFragment<FragmentInviteFriendBinding>() {

    // cho phép back
    override fun isCanBackPress() = true

    override fun getLayoutId() = R.layout.fragment_invite_friend

    override fun initView() {

    }

    override fun setAction() {
        binding.ivBack.onDebouncedClick {
            onBackPress()
        }
    }
}