package com.example.foodyappv2.common.widget.loading

import ai.ftech.dev.base.common.BaseDialog
import ai.ftech.dev.base.common.DialogConfig
import ai.ftech.dev.base.common.DialogScreen
import com.example.foodyappv2.R
import com.example.foodyappv2.databinding.LoadingAppDialogBinding


class LoadingDialog : BaseDialog<LoadingAppDialogBinding>() {

    var loading: String? = null

    override fun getConfig(): DialogConfig {
        return object : DialogConfig {
            override fun getLayoutResource() = R.layout.loading_app_dialog
            override fun getBackgroundId() = R.id.backgroundDialog
            override fun screen(): DialogScreen {
                return DialogScreen().apply {
                    isFullWidth = true
                    isFullHeight = true
                    isDismissByTouchOutSide = false
                    isDismissByOnBackPressed = false
                }
            }
        }
    }

    override fun onInitView() {
        loading?.let {
            binding.tvLoading.text = it
        }
    }
}
