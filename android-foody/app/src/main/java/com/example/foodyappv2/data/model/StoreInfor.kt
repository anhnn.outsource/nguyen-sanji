package com.example.foodyappv2.data.model

class StoreInfor(
    var name:String,
    var image: MutableList<Int>,
    var address: String,
    var rate: Int,
    var verified :String,
    var range :String,
    var timer :String,
    var category: String
) {
}