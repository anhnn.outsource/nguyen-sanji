package com.example.foodyappv2.domain.model.common

import com.example.foodyappv2.AppConfig

class DataPage<T> {
    var dataList: List<T>? = null
    var pageIndex: Int = 1
    var total: Int = 0
    var limit: Int = AppConfig.PAGE_LIMIT_DEFAULT

    fun hasLoadMore(): Boolean {
        return pageIndex * limit < total
    }
}
