package com.example.foodyappv2.data.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class ItemInCart() : Parcelable {
    @SerializedName("id")
    @Expose
    var id: String = ""

    @SerializedName("nameItem")
    @Expose
    var nameItem: String? = null

    @SerializedName("amount")
    @Expose
    var amount: Long? = null

    @SerializedName("total")
    @Expose
    var total: Long? = null

    constructor(parcel: Parcel) : this() {
        id = parcel.readString().toString()
        nameItem = parcel.readString()
        amount = parcel.readValue(Long::class.java.classLoader) as? Long
        total = parcel.readValue(Long::class.java.classLoader) as? Long
    }

//    constructor(parcel: Parcel) : this() {
//    }
//
//    override fun writeToParcel(parcel: Parcel, flags: Int) {
//
//    }
//
//    override fun describeContents(): Int {
//        return 0
//    }
//
//    companion object CREATOR : Parcelable.Creator<ItemInCart> {
//        override fun createFromParcel(parcel: Parcel): ItemInCart {
//            return ItemInCart(parcel)
//        }
//
//        override fun newArray(size: Int): Array<ItemInCart?> {
//            return arrayOfNulls(size)
//        }
//    }
    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeString(nameItem)
        parcel.writeValue(amount)
        parcel.writeValue(total)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ItemInCart> {
        override fun createFromParcel(parcel: Parcel): ItemInCart {
            return ItemInCart(parcel)
        }

        override fun newArray(size: Int): Array<ItemInCart?> {
            return arrayOfNulls(size)
        }
    }
}