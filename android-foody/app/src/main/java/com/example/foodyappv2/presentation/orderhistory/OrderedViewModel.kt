package com.example.foodyappv2.presentation.orderhistory

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.foodyappv2.AppPreference
import com.example.foodyappv2.R
import com.example.foodyappv2.base.extension.getAppString
import com.example.foodyappv2.data.model.ItemInCart
import com.example.foodyappv2.data.model.Ordered
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query

class OrderedViewModel : ViewModel() {

    private var firestore: FirebaseFirestore = FirebaseFirestore.getInstance()

    var ordereds = MutableLiveData<List<Ordered>>()

    fun getData() {
        val productRefs = firestore.collection("ordered").orderBy("date", Query.Direction.DESCENDING)
        val results = mutableListOf<Ordered>()
        productRefs
            .get().addOnSuccessListener {
                it?.let { it ->
                    it.forEach {
                        if (it.get("owner").toString().equals(AppPreference.profile.id)) {
                            results.add(
                                Ordered().apply {
                                    id = it.get("id").toString()
                                    name = it.get("name").toString()
                                    total = it.get("total") as Long
                                    paymentMethod = it.get("paymentMethod").toString()
                                    amount = it.get("amount") as Long
                                    date = it.get("date").toString()
                                    status = it.get("status") as Long
                                    discount = it.get("discount") as Long

                                    var items: MutableList<HashMap<String, String>> =
                                        it.get("itemInCarts") as MutableList<HashMap<String, String>>
                                    var itemInCarts = mutableListOf<ItemInCart>()
                                    items.forEach {
                                        var itemInCart = ItemInCart().apply {
                                            this.id = it.get("id").toString()
                                            this.nameItem = it.get("nameItem").toString()
                                            this.amount = it.get("amount") as Long
                                            this.total = it.get("total") as Long
                                        }
                                        itemInCarts.add(itemInCart)
                                    }
                                    this.itemInCarts = itemInCarts

                                }
                            )
                        }

                    }
                    ordereds.postValue(results)
                }
            }.addOnFailureListener {

            }
    }

    fun updateOrder(
        ordered: Ordered,
        onSuccess: () -> Unit,
        onFail: (String) -> Unit,
        state: STATE_ORDERED
    ) {
        ordered.id?.let {
            when (ordered.status) {
                0L -> {
                    when (state) {
                        STATE_ORDERED.RECEIVED -> {
                            firestore.collection("ordered").document(it).update("status", 1L)
                                .addOnSuccessListener {
                                    onSuccess.invoke()
                                }.addOnFailureListener {
                                    Log.d("TAG", "Unsuccessfully updated!")
                                }
                        }

                        STATE_ORDERED.CANCEL -> {
                            firestore.collection("ordered").document(it).update("status", 2L)
                                .addOnSuccessListener {
                                    onSuccess.invoke()
                                }.addOnFailureListener {
                                    Log.d("TAG", "Unsuccessfully updated!")
                                }
                        }
                    }
                }
                1L -> {
                    onFail.invoke(getAppString(R.string.shiped))
                }

                2L -> {
                    onFail.invoke(getAppString(R.string.you_canceled))
                }

                else -> {
                    onFail.invoke(getAppString(R.string.failure))

                }
            }
        }
    }

    enum class STATE_ORDERED {
        RECEIVED,
        CANCEL
    }
}