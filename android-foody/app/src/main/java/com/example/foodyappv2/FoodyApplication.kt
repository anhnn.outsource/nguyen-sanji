package com.example.foodyappv2

import ai.ftech.dev.base.common.BaseApplication
import android.annotation.SuppressLint
import com.example.foodyappv2.utils.FirebaseUtils
import com.google.firebase.firestore.FirebaseFirestore
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class FoodyApplication : BaseApplication() {

    companion object {
        fun getApplication() = appInstance

        @SuppressLint("StaticFieldLeak")
        lateinit var application: FoodyApplication

    }

    override fun onCreate() {
        super.onCreate()

        application = this

        FirebaseUtils.fireStoreInit()

        startKoin {
            androidLogger()
            androidContext(this@FoodyApplication)
            modules(
//                    applicationModule,
//                    remoteModule,
//                    repositoryModule,
//                    actionModule,
//                    viewModelModule,
            )
        }

        AppPreferences.init(this)
    }
}
