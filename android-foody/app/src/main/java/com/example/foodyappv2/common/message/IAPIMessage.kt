package com.example.foodyappv2.common.message

import android.content.Context
import com.example.foodyappv2.domain.APIException

interface IAPIMessage {
    fun getAPIMessage(context: Context?, exception: APIException): String?
}
