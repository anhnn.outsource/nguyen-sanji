package com.example.foodyappv2.presentation.search

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.foodyappv2.data.repository.SearchRepository
import com.example.foodyappv2.data.source.local.SearchEntity


class SearchViewModel : ViewModel() {

    private val repository = SearchRepository()
    val listSearchLiveData = MutableLiveData<List<SearchEntity>>()
    val listSearchLiveData2 = repository.getRecentSearchLiveData()
    var keySearch = MutableLiveData<String>("")

    fun insert(searchEntity: SearchEntity) {
        repository.insert(searchEntity)
    }

    fun getRecentSearch() {
        listSearchLiveData.value = repository.getRecentSearch()
    }


}