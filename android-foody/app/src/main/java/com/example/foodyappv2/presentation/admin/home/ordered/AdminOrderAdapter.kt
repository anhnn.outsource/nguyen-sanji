package com.example.foodyappv2.presentation.admin.home.ordered

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.foodyappv2.R
import com.example.foodyappv2.base.extension.getAppColor
import com.example.foodyappv2.data.model.Ordered
import com.example.foodyappv2.data.source.local.ItemInCartEntity
import com.example.foodyappv2.databinding.ItemAdminOrderBinding
import com.example.foodyappv2.presentation.cart.CartAdapter

class AdminOrderAdapter : RecyclerView.Adapter<AdminOrderAdapter.AdminOrderViewHolder>() {

    private var list: MutableList<Ordered> = mutableListOf()

    @SuppressLint("NotifyDataSetChanged")
    fun setData(data: List<Ordered>) {
        list.clear()
        list.addAll(data)
        notifyDataSetChanged()
    }

    override fun getItemViewType(position: Int): Int {
        return super.getItemViewType(position)
    }

    inner class AdminOrderViewHolder(val binding: ItemAdminOrderBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun onBind(index: Int, item: Ordered) {

            val adapter = CartAdapter()
            var itemsInCarts = mutableListOf<ItemInCartEntity>()
            item.itemInCarts?.forEach {
                var item = ItemInCartEntity(
                    idFood = it.id,
                    amount = it.amount?.toInt() ?: 0,
                    nameItem = it.nameItem.toString(),
                    total = it.total?.toInt() ?: 0
                )

                itemsInCarts.add(item)
            }

            adapter.list = itemsInCarts
            binding.rvAdminItemInCart.adapter = adapter
            adapter.notifyDataSetChanged()

            binding.tvAdminTotal.text = item.total.toString() + " $"
            binding.tvAdminDate.text = item.date.toString()
            binding.tvAdminStatus.text = item.getStatus()
            binding.tvAdminDetail.text = item.paymentMethod
            binding.tvAdminVoucher.text = "Discount: ${item.discount.toString()} $"

            when (item.status) {
                0L -> {
                    binding.ivAdminStatus.setImageResource(R.drawable.ic_processing)
                    binding.tvAdminStatus.setTextColor(getAppColor(R.color.yellow))
                }
                1L -> {
                    binding.ivAdminStatus.setImageResource(R.drawable.ic_check)
                    binding.tvAdminStatus.setTextColor(getAppColor(R.color.light_blue))
                }
                else -> {
                    binding.ivAdminStatus.setImageResource(R.drawable.ic_warn)
                    binding.tvAdminStatus.setTextColor(getAppColor(R.color.red))
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AdminOrderViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = DataBindingUtil.inflate<ItemAdminOrderBinding>(
            layoutInflater,
            R.layout.item_admin_order,
            parent,
            false
        )
        return AdminOrderViewHolder(binding)
    }

    override fun onBindViewHolder(holder: AdminOrderViewHolder, position: Int) {
        val item = list[position]

        holder.onBind(position, item)

    }

    override fun getItemCount() = list.size

}