package com.example.foodyappv2.data.source.remote.network

import android.util.Log
import retrofit2.Retrofit
import com.example.foodyappv2.data.source.remote.network.config.FoodyRetrofitConfig
import java.util.concurrent.ConcurrentHashMap

object RetrofitFactory {

    private val TAG = RetrofitFactory::class.java.simpleName
    private const val FOODY = "FOODY"
    private const val LANGUAGE_EN = 1
    private const val LANGUAGE_VI = 2

    private val builderMap = ConcurrentHashMap<String, RetrofitBuilderInfo>()

    fun <T> createFoodyService(service: Class<T>): T? {
        synchronized(RetrofitBuilderInfo::class.java) {
            var builderInfo = builderMap[FOODY]

            if (builderInfo == null) {
                Log.d(TAG, "Create new domain retrofit builder for ${ApiConfig.BASE_URL_FOODY}")
                builderInfo = RetrofitBuilderInfo()
                builderInfo.builder = FoodyRetrofitConfig().getRetrofitBuilder()
                builderMap[FOODY] = builderInfo
            }
            Log.e(TAG, "Reuse domain retrofit builder for ${ApiConfig.BASE_URL_FOODY}")
            return builderInfo.builder?.build()?.create(service)
        }
    }

    class RetrofitBuilderInfo {
        var builder: Retrofit.Builder? = null
        var token: String? = null
        var uid: String? = null
        var language: LANGUAGE? = null

        fun valid(token: String, uid: String): Boolean {
            return this.token == token && this.uid == uid
        }
    }

    enum class LANGUAGE(val value: String) {
        VI("vi"),
        EN("en"),
        ZH("zh"),
    }
}
