package com.example.foodyappv2.presentation.voucher

import androidx.fragment.app.activityViewModels
import com.example.foodyappv2.R
import com.example.foodyappv2.base.BaseFragment
import com.example.foodyappv2.databinding.FragmentVoucherBinding
import com.example.foodyappv2.presentation.shareviewmodel.FoodViewModel
import com.example.foodyappv2.utils.onDebouncedClick


class VoucherFragment : BaseFragment<FragmentVoucherBinding>(){

    private val foodlViewModel: FoodViewModel by activityViewModels()

    override fun isCanBackPress() = true

    override fun getLayoutId() = R.layout.fragment_voucher

    override fun initView() {
        binding.item = foodlViewModel.liveVoucher.value
    }

    override fun setAction() {
        binding.ivBack.onDebouncedClick {
            onBackPress()
        }
    }

}