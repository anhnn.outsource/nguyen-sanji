package com.example.foodyappv2.data.model

class Voucher(
    var content:String,
    var image: Int,
    var description: String,
    var code:String,
    var owner: String,
    var disscount:Int
) {
}