package com.example.foodyappv2.common

import ai.ftech.dev.base.common.ADJUST_RESIZE_MODE
import ai.ftech.dev.base.common.BaseActivity
import ai.ftech.dev.base.common.BaseFragment
import ai.ftech.dev.base.eventbus.IEventHandler
import com.example.foodyappv2.base.extension.observer
import android.content.Context
import android.view.View
import android.widget.Toast
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.LiveData
import com.example.foodyappv2.common.action.AppActionCallback
import com.example.foodyappv2.common.action.AppActionResult
import com.example.foodyappv2.common.widget.headeralert.HEADER_ALERT_TIME_SHOWN
import com.example.foodyappv2.common.widget.headeralert.HEADER_ALERT_TYPE
import com.example.foodyappv2.presentation.main.MainActivity

abstract class FoodyFragment<DB : ViewDataBinding> :
    BaseFragment<DB>(),
    IFoodyContext,
    IEventHandler {

//    protected val mainViewModel by activityViewModels<MainViewModel>()

    val activityOwner by lazy {
        requireActivity() as MainActivity
    }

    override fun onDestroyView() {
        super.onDestroyView()
        if (getConfig().isSoftInputAdjustResize()) {
            adjustResize(ADJUST_RESIZE_MODE.SOFT_INPUT_ADJUST_NOTHING)
        }
    }

    override fun onPrepareInitView() {
        super.onPrepareInitView()
    }

    override fun onInitView() {
        if (getConfig().isSoftInputAdjustResize()) {
            adjustResize(ADJUST_RESIZE_MODE.SOFT_INPUT_ADJUST_RESIZE)
        } else {
            adjustResize(ADJUST_RESIZE_MODE.SOFT_INPUT_ADJUST_NOTHING)
        }
    }

    override fun onObserverViewModel() {
        super.onObserverViewModel()
    }

    override fun onViewClick() {
        super.onViewClick()
    }

    override fun onBackPressed(tag: String?) {
        super.onBackPressed(tag)
    }

    override fun getActivityContext(): Context {
        return requireActivity() as MainActivity
    }

    override fun showLoading(message: String) {
        activityOwner.showLoading(message)
    }

    override fun hideLoading() {
        activityOwner.hideLoading()
    }

    override fun showError(message: String?) {
//        showHeaderAlert(message, HEADER_ALERT_TYPE.ERROR, HEADER_ALERT_TIME_SHOWN.DELAY_2_SECOND)
    }

    override fun showHeaderAlert(
        msg: String?,
        type: HEADER_ALERT_TYPE,
        timeShown: HEADER_ALERT_TIME_SHOWN
    ) {
        if (activity != null && activity is FoodyActivity<*>) {
            (activity as FoodyActivity<*>).showHeaderAlert(msg, type, timeShown)
        }
    }

//    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
//    override fun onEvent(event: IEvent) {
//
//    }

    fun hideKeyBoardAndClearFocus(vararg views: View) {
        views.forEach {
            it.apply {
                clearFocus()
                activityOwner.hideKeyBoard(this)
            }
        }
    }

    fun showToast(message: String?) {
        Toast.makeText(activityOwner, message, Toast.LENGTH_SHORT).show()
    }

    fun doRequestPermission(
        permission: Array<String>,
        listener: BaseActivity.PermissionListener
    ) {
        activityOwner.doRequestPermission(permission, listener)
    }

    protected fun <T> observerViewModelAction(
        liveAction: LiveData<AppActionResult<T>>,
        callback: AppActionCallback<T>
    ) {
        observer(liveAction) {
            if (it?.resultStatus == AppActionResult.RESULT_STATUS.ERROR) {
                callback.onException(it.exception)
                return@observer
            } else if (it?.resultStatus == AppActionResult.RESULT_STATUS.SUCCESS) {
                callback.onSuccess(it.data)
                return@observer
            }
        }
    }

    private fun adjustResize(mode: ADJUST_RESIZE_MODE) {
        activityOwner.window.setSoftInputMode(mode.value)
    }
}
