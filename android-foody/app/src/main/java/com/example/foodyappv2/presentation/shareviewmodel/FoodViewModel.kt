package com.example.foodyappv2.presentation.shareviewmodel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.foodyappv2.data.model.StoreInfor
import com.example.foodyappv2.data.model.Food
import com.example.foodyappv2.data.model.Store
import com.example.foodyappv2.data.model.Voucher
import com.example.foodyappv2.data.repository.DataResponseRepository
import com.example.foodyappv2.utils.FirebaseUtils
import com.google.firebase.firestore.FirebaseFirestore

class FoodViewModel : ViewModel() {

    var firestore = FirebaseUtils.getFireStore()

    private val TAG = "FoodDetailViewModel"
    private val dataResponseRepository = DataResponseRepository()

    val foodsLiveData = MutableLiveData<MutableList<Food>>()
    val storesLiveData = MutableLiveData<MutableList<Store>>()

    val currentFoodsLiveData = MutableLiveData<MutableList<Food>>()
    var tempAmountFood = MutableLiveData<Int>(0)

    val liveFood = MutableLiveData<Food>()
    val liveBigFood = MutableLiveData<Store>()
    val liveVoucher = MutableLiveData<Voucher>()

    var amount = MutableLiveData<Int>(0)
    var total = MutableLiveData<Int>(0)

    fun upAmount(v: Int = 1) {
        amount.value = amount.value?.plus(v)
        total.value = liveFood.value?.getPriceToInt()?.let { total.value?.plus(it) }
    }

    fun downAmount(v: Int = 1) {
        if (amount.value!! > 1) {
            amount.value = amount.value?.minus(v)
            total.value = liveFood.value?.getPriceToInt()?.let { total.value?.minus(it) }
        }
    }

    fun resetAmount() {
        amount.value = 1
        total.value = liveFood.value?.getPriceToInt()
    }

    fun loadMore(value: Int) {
        val newList = currentFoodsLiveData.value!!
        val fullList = foodsLiveData.value!!
        val currSize = currentFoodsLiveData.value?.size!!
        var des = 0
        if(currSize != fullList.size) {
            des = if (currSize + value >= fullList.size) {
                fullList.size
            } else {
                currSize + value
            }
            for (i in currSize until des) {
                newList.add(fullList[i])
            }
            currentFoodsLiveData.value = newList
        }
    }

    fun getAllFood() {
        val productRefs = firestore.collection("foods")
        val foods = mutableListOf<Food>()
        productRefs
            .get().addOnSuccessListener {
                it?.let { it ->
                    it.forEach {
                        foods.add(
                            Food(
                                it.get("id").toString(),
                                it.get("name").toString(),
                                it.get("price").toString(),
                                it.get("image").toString(),
                                it.get("idStore").toString()
                            )
                        )
                    }
                }

                currentFoodsLiveData.value = foods
                foodsLiveData.value = foods

            }.addOnFailureListener {

            }
    }
    fun getAllStore() {
        val productRefs = firestore.collection("stores")
        val stores = mutableListOf<Store>()
        productRefs
            .get().addOnSuccessListener {
                it?.let { it ->
                    it.forEach {
                        stores.add(
                            Store(
                                it.get("id").toString(),
                                it.get("name").toString(),
                                it.get("address").toString(),
                                it.get("rate").toString(),
                                mutableListOf(it.get("image").toString()),
                                it.get("category").toString()
                            )
                        )
                    }
                }

                storesLiveData.value = stores

            }.addOnFailureListener {

            }
    }
    fun resetCurrentFoods() {
        currentFoodsLiveData.value?.clear()
    }


}


