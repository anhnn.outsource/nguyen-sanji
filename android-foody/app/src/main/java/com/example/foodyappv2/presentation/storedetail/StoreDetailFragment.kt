package com.example.foodyappv2.presentation.storedetail

import android.os.Bundle
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearSnapHelper
import com.example.foodyappv2.R
import com.example.foodyappv2.base.BaseFragment
import com.example.foodyappv2.data.model.Food
import com.example.foodyappv2.databinding.FragmentStoreDetailBinding
import com.example.foodyappv2.presentation.shareviewmodel.FoodViewModel
import com.example.foodyappv2.presentation.home.FoodAdapterVer
import java.util.*

class StoreDetailFragment : BaseFragment<FragmentStoreDetailBinding>(),
    StoreImageAdapter.FoodImageListener,
    FoodAdapterVer.FoodListener{

    private var storeImageAdapter = StoreImageAdapter()
    private var foodAdapter = FoodAdapterVer()
    private var foodImageList = mutableListOf<String>()

    private val foodsViewModel: FoodViewModel by activityViewModels()
    override fun getLayoutId(): Int {
        return R.layout.fragment_store_detail
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
        }
    }

    override fun isCanBackPress() = true

    override fun initView() {
        val snapHelper = LinearSnapHelper()
        snapHelper.attachToRecyclerView(binding.rvStoreDetailFood)

        foodsViewModel.liveBigFood.observe(viewLifecycleOwner) { food ->
            binding.item = food
            foodImageList = food.image
            storeImageAdapter.list = food.image

        }

        storeImageAdapter.callBack = this
        binding.rvStoreDetailImageFood.adapter = storeImageAdapter

        foodAdapter.list = performFilter(foodsViewModel.liveBigFood.value?.id!!)
        foodAdapter.callBack = this
        binding.rvStoreDetailFood.adapter = foodAdapter

    }

    private fun performFilter(keySearch: String) : MutableList<Food>{
        var foodsTemp = mutableListOf<Food>()
        if (keySearch != "") {
            for (d in foodsViewModel.foodsLiveData.value!!) {
                if (d.idStore.lowercase(Locale.getDefault())
                        .contains(keySearch.lowercase(Locale.getDefault()))
                ) {
                    foodsTemp.add(d)
                }
            }
        } else {
            foodAdapter.list = foodsTemp
        }
        return foodsTemp
    }

    override fun setAction() {
        binding.ivStoreDetailBack.setOnClickListener {
            onBackPress()
        }
        binding.tvStoreDetailName.setOnClickListener {

        }
        binding.llRate.setOnClickListener {
            findNavController().navigate(R.id.action_bigFoodDetailFragment_to_ratingBottomSheet)
        }

    }

    override fun onItemClick(index: Int, item: String) {
        findNavController().navigate(R.id.action_bigFoodDetailFragment_to_imageBigFoodDetailFragment)
    }

    override fun onItemClick(index: Int, item: Food) {
        foodsViewModel.liveFood.value = (item)
        foodsViewModel.amount.value = 1
        foodsViewModel.total.value = item.getPriceToInt()
        findNavController().navigate(R.id.action_bigFoodDetailFragment_to_oderDetailFragment)
    }

}