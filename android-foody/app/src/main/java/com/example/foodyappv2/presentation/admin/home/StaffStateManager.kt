package com.example.foodyappv2.presentation.admin.home

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.example.foodyappv2.base.BaseFragment

class StaffStateManager(fragmentManager: FragmentManager) :
    FragmentStatePagerAdapter(fragmentManager, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    private var tabList = mutableListOf<Fragment>()
    private var tabTitle = mutableListOf<String>()

    override fun getCount() = tabList.size

    override fun getItem(position: Int) = tabList[position]

    override fun getPageTitle(position: Int): CharSequence {
        return tabTitle[position]
    }

    fun addTab(fragmentList: List<Fragment>) {
        this.tabList.clear()
        this.tabList.addAll(fragmentList)
        notifyDataSetChanged()
    }

    fun addTabTitle(titleList: List<String>) {
        this.tabTitle.clear()
        this.tabTitle.addAll(titleList)
        notifyDataSetChanged()
    }


}