package com.example.foodyappv2.presentation.register

import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.foodyappv2.R
import com.example.foodyappv2.base.extension.getAppString
import com.example.foodyappv2.data.model.USER_ROLE
import com.example.foodyappv2.data.model.User
import com.example.foodyappv2.utils.FirebaseUtils

class RegisterViewModel : ViewModel() {

    private var firestore = FirebaseUtils.getFireStore()

    var userEmailLiveData = MutableLiveData<String>("")
    var userPassLiveData = MutableLiveData<String>("")
    var userPassAgainLiveData = MutableLiveData<String>("")
    val userPhoneNumberLivaData = MutableLiveData<String>("")

    fun isEmailValid(): Boolean {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(userEmailLiveData.value).matches()
    }

    fun isPassValid(): Boolean {
        return (userPassLiveData.value!!.length > 5)
    }

    fun isPhoneValid(): Boolean {
        return (userPhoneNumberLivaData.value!!.length == 10)
    }

    fun isPassValidCorrect(): Boolean {
        return (userPassLiveData.value.equals(userPassAgainLiveData.value!!))
    }

    fun isInforValid(): Boolean {
        return (isEmailValid() && isPassValid() && isPassValidCorrect() && isPhoneValid())
    }

    fun register(
        onSuccess: (msg: String) -> Unit,
        onFailure: (msg: String) -> Unit
    ) {
        val user = hashMapOf(
            "email" to userEmailLiveData.value,
            "pass" to userPassLiveData.value,
            "phone" to userPhoneNumberLivaData.value,
            "role" to USER_ROLE.USER.value,
        )

        val productRefs = firestore.collection("users")
        productRefs.get().addOnSuccessListener {
            var isAccountExits = false
            it?.let { it ->
                it.forEach {
                    if (it.get("email").toString().equals(userEmailLiveData.value)) {
                        isAccountExits = true
                    }
                }

                if (!isAccountExits) {
                    firestore.collection("users")
                        .add(user)
                        .addOnSuccessListener { documentReference ->
                            onSuccess.invoke(getAppString(R.string.success))
                        }.addOnFailureListener { e ->
                            e.message?.let { onFailure.invoke(it) }
                        }
                } else {
                    onFailure.invoke("Account đã tồn tại")
                }
            }
        }.addOnFailureListener {
            onFailure.invoke(it.message.toString())
        }
    }
}