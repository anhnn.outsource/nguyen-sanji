package com.example.foodyappv2.common.model

interface ISelectionItem<T> {
    var isSelected: Boolean
    fun getData(): T
    fun getTitle(): String = ""
    fun itemIsTheSame(data: T?): Boolean = false
    fun match(keySearch: String): Boolean = false
    fun getUrlImage(): String? = null
    fun allowSelectAll(): Boolean = false
}
