package com.example.foodyappv2.data.model


import com.google.firebase.database.IgnoreExtraProperties

@IgnoreExtraProperties
data class User(
    val name: String? = null,
    val mail: String? = null,
    val avatar: String? = null,
    val role: USER_ROLE = USER_ROLE.USER,
    val phone: String? = null
) {
}

enum class USER_ROLE(val value: String) {
    ADMIN("admin"),
    USER("user")
}