package com.example.foodyappv2.utils

import com.example.foodyappv2.data.source.remote.FoodyService
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


class NetWorkUtils {

    companion object {
        private const val BASE_URL = "https://nodejs-my-food.herokuapp.com/"

        private var retrofit: Retrofit? = null

        fun getClient(baseUrl: String?): Retrofit? {
            if (retrofit == null) {
                retrofit = Retrofit.Builder()
                    .baseUrl(baseUrl)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()
            }
            return retrofit
        }

        fun getService() : FoodyService {
            return getClient(BASE_URL)?.create(FoodyService::class.java)!!
        }

    }


}