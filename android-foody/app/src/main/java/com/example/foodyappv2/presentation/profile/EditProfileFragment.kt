package com.example.foodyappv2.presentation.profile

import android.app.Activity.RESULT_OK
import android.content.ActivityNotFoundException
import android.content.Intent
import android.graphics.Bitmap
import android.provider.MediaStore
import com.example.foodyappv2.AppPreference
import com.example.foodyappv2.R
import com.example.foodyappv2.base.BaseFragment
import com.example.foodyappv2.databinding.FragmentEditProfileBinding
import com.example.foodyappv2.common.imageloader.ImageLoaderFactory
import com.example.foodyappv2.utils.onDeBoundClick
import com.example.foodyappv2.utils.onDebouncedClick


class EditProfileFragment : BaseFragment<FragmentEditProfileBinding>() {

    private val loader = ImageLoaderFactory.glide()
    private val REQUEST_IMAGE_CAPTURE = 1

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            val imageBitmap = data?.extras?.get("data") as Bitmap
            AppPreference.profile.avatarBitmap = imageBitmap
            loader.loadCircleImage(binding.root, imageBitmap, binding.ivEditProfileAvatar)
        }
    }

    override fun isCanBackPress() = true

    override fun getLayoutId() = R.layout.fragment_edit_profile

    override fun initView() {
        with(binding) {
            edtMail.setText(AppPreference.profile.email)
            AppPreference.profile.name?.let {
                edtName.setText(it)
            }
        }
        AppPreference.profile.avatarBitmap?.let {
            loader.loadCircleImage(binding.root, it, binding.ivEditProfileAvatar)
        }

    }

    override fun setAction() {
        with(binding) {
            ivBack.onDebouncedClick {
                onBackPress()
            }

            tvUpdate.onDebouncedClick {
                with(binding) {
                    edtName.text?.let {
                        AppPreference.profile.name = it.toString()
                    }

                }
                onBackPress()
            }

            tvOut.onDebouncedClick {
                onBackPress()
            }

            ivEditProfileAvatar.onDeBoundClick {
                dispatchTakePictureIntent()
            }

            isFocusName = edtName.isFocusable
            isFocusMail = edtMail.isFocusable
            isFocusPass = edtPass.isFocusable

        }
    }

    private fun dispatchTakePictureIntent() {
        val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        try {
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE)
        } catch (e: ActivityNotFoundException) {

        }
    }
}
