package com.example.foodyappv2.data.model

import com.google.gson.annotations.SerializedName


class Food(
    @SerializedName("id")
    var id: String,
    @SerializedName("name")
    var name: String,
    @SerializedName("price")
    var price: String,
    @SerializedName("image")
    var image: String,
    @SerializedName("idStore")
    var idStore: String = ""
) {
    fun getPriceToInt() = price.toInt()
}