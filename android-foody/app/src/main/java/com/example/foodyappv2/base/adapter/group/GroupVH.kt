package com.example.foodyappv2.base.adapter.group

import com.example.foodyappv2.base.adapter.BaseViewHolder
import android.view.View

internal open class GroupVH<T, GD : GroupData<*>>(itemView: View) : BaseViewHolder<T>(itemView) {
    var groupData: GD? = null
}