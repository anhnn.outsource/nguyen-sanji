package com.example.foodyappv2.data.repository

import android.util.Log
import com.example.foodyappv2.data.model.Food
import com.example.foodyappv2.data.modelserver.DataResponse
import com.example.foodyappv2.utils.NetWorkUtils

import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class DataResponseRepository {
    private val TAG = "DataRepository"

    private val apiService = NetWorkUtils.getService()

    fun getFoodList(
        onSuccess:(foods: MutableList<Food>)-> Unit,
        onError: (t: Throwable?)-> Unit
    ) {
        apiService.getFoodList().enqueue(object : Callback<DataResponse> {
            override fun onResponse(call: Call<DataResponse>, response: Response<DataResponse>) {
                if (response.isSuccessful){
                    Log.d(TAG, "onResponse: get response success")
                    onSuccess.invoke(response.body()?.content?.datalist!!)
                } else {
                    Log.d(TAG, "onResponse: get response fail")
                    onError.invoke(null)
                }
            }
            override fun onFailure(call: Call<DataResponse>, t: Throwable) {
                Log.d(TAG, "onFailure: enqueue fail")
                onError.invoke(t)
            }
        })
    }
}

