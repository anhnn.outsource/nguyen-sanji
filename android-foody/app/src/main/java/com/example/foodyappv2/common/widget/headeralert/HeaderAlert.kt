package com.example.foodyappv2.common.widget.headeralert

import com.example.foodyappv2.base.extension.getAppString
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.view.animation.LayoutAnimationController
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import com.example.foodyappv2.R
import com.example.foodyappv2.base.extension.getAppDrawable
import com.example.foodyappv2.common.FoodyActivity

interface IHeaderAlert {
    fun show(msg: String?, type: HEADER_ALERT_TYPE)
    fun dismiss()
    fun destroy()
}

enum class HEADER_ALERT_TYPE {
    ERROR, WARNING, SUCCESS
}

enum class HEADER_ALERT_TIME_SHOWN(val time: Long) {
    DELAY_DEFAULT(2000L),
    DELAY_1_HALF_SECOND(1500L),
    DELAY_2_SECOND(2000L),
    DELAY_3_SECOND(3000L)
}

abstract class HeaderAlert(val activity: FoodyActivity<*>) : IHeaderAlert {

    var message: String? = null
    var alertView: ViewGroup? = null
    private var isShowing = false

    override fun show(msg: String?, type: HEADER_ALERT_TYPE) {
        Log.e("HeaderAlert", "message: $msg $isShowing")
        if (isShowing) {
            if (msg != message) {
                enqueueMessage(msg, type)
            }
            return
        }

        isShowing = true
        message = msg
        if (alertView == null) {
            alertView = onCreateView(activity, type)
            addView(alertView!!)
        }

        onViewCreated(alertView!!, type)
        showAnim(alertView!!, type)
    }

    override fun dismiss() {
        message = null
        dismissAnim(alertView!!)
    }

    abstract fun onCreateView(activity: FoodyActivity<*>, type: HEADER_ALERT_TYPE): ViewGroup
    abstract fun onViewCreated(view: ViewGroup, type: HEADER_ALERT_TYPE)

    open fun addView(view: ViewGroup) {
        activity.window.addContentView(alertView,
                ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT))
    }

    open fun showAnim(view: ViewGroup, type: HEADER_ALERT_TYPE) {
        val animDown = AnimationUtils.loadAnimation(activity, R.anim.slide_in_bottom)
        val animController = LayoutAnimationController(animDown)

        view.visibility = View.VISIBLE
        view.layoutAnimation = animController
        animDown.setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationRepeat(animation: Animation?) {
            }

            override fun onAnimationEnd(animation: Animation?) {
                onShow(type)
            }

            override fun onAnimationStart(animation: Animation?) {
            }
        })
        view.startAnimation(animDown)

        /* view.visibility = View.VISIBLE
         view.post {
             Log.e("HeaderAlert", "onSHow")
             view.translationY = -view.height.toFloat()
             view.animate().translationYBy(view.height.toFloat())
         }*/
    }

    open fun dismissAnim(view: ViewGroup) {
        val animUp = AnimationUtils.loadAnimation(activity, R.anim.slide_out_top)
        val animController = LayoutAnimationController(animUp)

        view.visibility = View.GONE
        view.layoutAnimation = animController
        animUp.setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationRepeat(animation: Animation?) {

            }

            override fun onAnimationEnd(animation: Animation?) {
                isShowing = false
                onDismiss()
            }

            override fun onAnimationStart(animation: Animation?) {
            }

        })
        view.startAnimation(animUp)
    }

    open fun onShow(type: HEADER_ALERT_TYPE) {}
    open fun onDismiss() {}
    open fun enqueueMessage(msg: String?, type: HEADER_ALERT_TYPE) {}
}


open class HeaderAlertDefault(activity: FoodyActivity<*>) : HeaderAlert(activity) {

    var timeShown = HEADER_ALERT_TIME_SHOWN.DELAY_2_SECOND
    private val messageEnqueue = arrayListOf<MessageInfo>()
    private val handler = Handler(Looper.getMainLooper())
    private val dismissRunnable = Runnable {
        dismiss()
    }

    override fun onCreateView(activity: FoodyActivity<*>, type: HEADER_ALERT_TYPE): ViewGroup {
        return LayoutInflater.from(activity).inflate(R.layout.header_alert_layout, null) as ViewGroup
    }

    override fun onViewCreated(view: ViewGroup, type: HEADER_ALERT_TYPE) {
        if ((activity.window.decorView.systemUiVisibility == View.SYSTEM_UI_FLAG_LAYOUT_STABLE or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN)
                || (activity.window.decorView.systemUiVisibility == View.SYSTEM_UI_FLAG_LAYOUT_STABLE or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN or View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR)) {
//            view.setPadding(view.paddingLeft, view.resources.getDimensionPixelSize(R.dimen.dimen_20) + activity.getStatusBarHeight(), view.paddingRight, view.paddingBottom)
        }
        val constBackground = view.findViewById<ConstraintLayout>(R.id.constHeaderAlertBackground)
        val ivIcon = view.findViewById<ImageView>(R.id.ivHeaderAlertIcon)
        val tvTitle = view.findViewById<TextView>(R.id.tvHeaderAlertTitle)
        val tvContent = view.findViewById<TextView>(R.id.tvHeaderAlertContent)

        if (type == HEADER_ALERT_TYPE.ERROR || type == HEADER_ALERT_TYPE.WARNING) {
            constBackground.setBackgroundResource(R.color.color_failure)
//            ivIcon.setImageResource(R.drawable.ic_error_info)
            tvTitle.text = getAppString(R.string.failure)
        } else {
            constBackground.setBackgroundResource(R.color.color_success)
//            ivIcon.setImageResource(R.drawable.ic_check)
            ivIcon.setImageDrawable(getAppDrawable(R.drawable.ic_header_alert_success))
            tvTitle.text = getAppString(R.string.success)
        }
        tvContent.text = message
        handler.postDelayed(dismissRunnable, timeShown.time)
    }

    override fun onDismiss() {
        super.onDismiss()
        Log.e("HeaderAlert", "onDismiss: ${messageEnqueue.size}")
        if (messageEnqueue.isNotEmpty()) {
            val nextMsg = messageEnqueue.removeAt(0)
            show(nextMsg.msg, nextMsg.type!!)
        }
    }

    /*override fun show(msg: String?, type: HEADER_ALERT_TYPE) {
        handler.removeCallbacks(dismissRunnable)
        super.show(msg, type)
    }*/

    override fun destroy() {
        handler.removeCallbacks(dismissRunnable)
        messageEnqueue.clear()
    }

    override fun enqueueMessage(msg: String?, type: HEADER_ALERT_TYPE) {
        super.enqueueMessage(msg, type)
        var exist = false
        messageEnqueue.forEach {
            if (it.isSame(msg)) {
                exist = true
                return@forEach
            }
        }
        if (!exist) {
            val msgInfo = MessageInfo()
            msgInfo.msg = msg
            msgInfo.type = type
            messageEnqueue.add(msgInfo)
        }
    }


    private class MessageInfo() {
        var type: HEADER_ALERT_TYPE? = null
        var msg: String? = null

        fun isSame(msg: String?): Boolean {
            return this.msg == msg
        }
    }
}


fun FoodyActivity<*>.showHeaderAlert(code: Int, msg: String?) {
    when (code) {
//        422 -> if (!msg.isNullOrEmpty()) {
//            this.showHeaderAlert(msg, HEADER_ALERT_TYPE.ERROR)
//        }
//        403 -> {
//            if (!msg.isNullOrEmpty()) {
//                this.showHeaderAlert(msg, HEADER_ALERT_TYPE.ERROR)
//            } else {
//                this.showHeaderAlert(getString(R.string.msg_403), HEADER_ALERT_TYPE.ERROR)
//            }
//        }
//        404 -> {
//            if (!msg.isNullOrEmpty()) {
//                this.showHeaderAlert(msg, HEADER_ALERT_TYPE.ERROR)
//            } else {
//                this.showHeaderAlert(getString(R.string.msg_404), HEADER_ALERT_TYPE.ERROR)
//            }
//        }
//        500 -> {
//            if (!msg.isNullOrEmpty()) {
//                this.showHeaderAlert(msg, HEADER_ALERT_TYPE.ERROR)
//            } else {
//                this.showHeaderAlert(getString(R.string.msg_500), HEADER_ALERT_TYPE.ERROR)
//            }
//        }
//
//        else -> {
//            if (!msg.isNullOrEmpty()) {
//                this.showHeaderAlert(msg, HEADER_ALERT_TYPE.ERROR)
//            } else {
//                this.showHeaderAlert(getString(R.string.msg_500), HEADER_ALERT_TYPE.ERROR)
//            }
//        }
    }
}
