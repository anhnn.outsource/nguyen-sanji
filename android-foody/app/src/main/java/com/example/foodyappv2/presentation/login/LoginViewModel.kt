package com.example.foodyappv2.presentation.login

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.foodyappv2.AppPreference
import com.example.foodyappv2.R
import com.example.foodyappv2.base.extension.getAppString
import com.example.foodyappv2.data.model.Food
import com.example.foodyappv2.data.model.USER_ROLE
import com.example.foodyappv2.data.model.User
import com.example.foodyappv2.utils.FirebaseUtils
import com.google.firebase.auth.FirebaseAuth

class LoginViewModel : ViewModel() {

    private var firebaseAuth = FirebaseUtils.getFirebaseAuth()
    private var firestore = FirebaseUtils.getFireStore()

    var user: MutableLiveData<User>? = null

    fun login(
        email: String,
        pass: String,
        onSuccess: (msg: String, USER_ROLE) -> Unit,
        onFailure: (msg: String) -> Unit
    ) {
        val productRefs = firestore.collection("users")
        productRefs
            .get().addOnSuccessListener {
                it?.let { it ->
                    it.forEach {
                        if (email.equals(it.get("email")) && pass.equals(it.get("pass"))) {
                            user?.value = User(
                                it.get("name").toString(),
                                it.get("email").toString(),
                                it.get("avt").toString(),
                                role = when (it.get("role")) {
                                    "admin" -> USER_ROLE.ADMIN
                                    "user" -> USER_ROLE.USER
                                    else -> USER_ROLE.USER
                                },
                                it.get("phone").toString()
                            )

                            AppPreference.profile.id = it.id
                            onSuccess.invoke(
                                getAppString(R.string.success), when (it.get("role")) {
                                    "admin" -> USER_ROLE.ADMIN
                                    "user" -> USER_ROLE.USER
                                    else -> USER_ROLE.USER
                                }
                            )
                            return@addOnSuccessListener
                        }
                    }
                    onFailure.invoke(getAppString(R.string.failure))
                }
            }.addOnFailureListener {
                it.message?.let { it1 -> onFailure.invoke(it1) }
            }
    }


}
