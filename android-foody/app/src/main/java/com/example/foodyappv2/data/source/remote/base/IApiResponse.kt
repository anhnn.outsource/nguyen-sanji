package com.example.foodyappv2.data.source.remote.base

import ai.ftech.dev.base.common.converter.IConverter

interface IApiResponse {

    open fun isSuccessful(): Boolean {
        return true
    }

    open fun <S, D> convert(source: S, converter: IConverter<S, D>): D? {
        return converter.convert(source)
    }
}