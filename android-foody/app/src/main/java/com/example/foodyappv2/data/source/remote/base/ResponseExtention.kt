package com.example.foodyappv2.data.source.remote.base

import com.example.foodyappv2.domain.APIException
import okhttp3.Headers
import retrofit2.Call
import com.example.foodyappv2.data.source.remote.network.RetrofitFactory
import java.net.SocketTimeoutException
import java.net.UnknownHostException

fun <RESPONSE : IApiResponse, RETURN_VALUE> Call<RESPONSE>.invokeApi(
    block: (Headers, RESPONSE) -> RETURN_VALUE
): RETURN_VALUE {

    try {
        val response = this.execute()
        if (response.isSuccessful) {
            val body: RESPONSE? = response.body()
            if (body != null) {
                if (body.isSuccessful()) {
                    return block(response.headers(), body)
                } else {
                    throw ExceptionHelper.throwException(response)
                }
            } else {
                throw APIException(APIException.UNKNOWN_ERROR)
            }
        } else {
            throw ExceptionHelper.throwException(response)
        }
    } catch (e: Exception) {
        when (e) {
            is APIException -> {
                throw e
            }
            is UnknownHostException -> {
                throw APIException(APIException.NETWORK_ERROR)
            }
            is SocketTimeoutException -> {
                throw APIException(APIException.TIME_OUT_ERROR)
            }
            else -> {
                throw APIException(APIException.UNKNOWN_ERROR)
            }
        }
    }
}

fun <T : IApiService> invokeFoodyService(service: Class<T>): T {
    return RetrofitFactory.createFoodyService(
        service
    ) ?: throw APIException(APIException.UNKNOWN_ERROR)
}
