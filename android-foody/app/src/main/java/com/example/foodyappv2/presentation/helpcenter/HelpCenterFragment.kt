package com.example.foodyappv2.presentation.helpcenter

import com.example.foodyappv2.R
import com.example.foodyappv2.base.BaseFragment
import com.example.foodyappv2.databinding.FragmentHelpCenterBinding
import com.example.foodyappv2.utils.onDebouncedClick

class HelpCenterFragment : BaseFragment<FragmentHelpCenterBinding>() {

    override fun isCanBackPress() = true

    override fun getLayoutId() = R.layout.fragment_help_center

    override fun initView() {

    }

    override fun setAction() {
        binding.ivBack.onDebouncedClick {
            onBackPress()
        }
    }
}