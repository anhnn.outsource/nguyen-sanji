package com.example.foodyappv2.domain.model

import android.graphics.Bitmap
import android.os.Parcel
import android.os.Parcelable

class Profile() : Parcelable{
    var id: String? = null
    var email: String? = null
    var name: String? = ""
    var phone: String? = null
    var avatarBitmap: Bitmap? = null

    constructor(parcel: Parcel) : this() {
        id= parcel.readString()
        email = parcel.readString()
        name = parcel.readString()
        phone = parcel.readString()
        avatarBitmap = parcel.readParcelable(Bitmap::class.java.classLoader)
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeString(email)
        parcel.writeString(name)
        parcel.writeString(phone)
        parcel.writeParcelable(avatarBitmap, flags)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Profile> {
        override fun createFromParcel(parcel: Parcel): Profile {
            return Profile(parcel)
        }

        override fun newArray(size: Int): Array<Profile?> {
            return arrayOfNulls(size)
        }
    }
}