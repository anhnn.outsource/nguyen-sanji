package com.example.foodyappv2.presentation.orderdetail

import android.os.Bundle
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.example.foodyappv2.R
import com.example.foodyappv2.base.BaseFragment
import com.example.foodyappv2.data.source.local.ItemInCartEntity
import com.example.foodyappv2.databinding.FragmentOrderDetailBinding
import com.example.foodyappv2.presentation.shareviewmodel.FoodViewModel
import com.example.foodyappv2.presentation.cart.CartViewModel


class OderDetailFragment : BaseFragment<FragmentOrderDetailBinding>() {

    private val TAG = "OderDetailFragment"
    private val foodViewModel: FoodViewModel by activityViewModels()
    private val cartViewModel: CartViewModel by activityViewModels()

    companion object{

    }

    override fun isCanBackPress() = true

    override fun initBinding() {
        binding.viewModel = foodViewModel
        binding.food = foodViewModel.liveFood.value
    }

    override fun getLayoutId(): Int {
        return R.layout.fragment_order_detail
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
        }
    }

    override fun setAction() {
        binding.imvZoomIn.setOnClickListener {
            foodViewModel.upAmount()
        }
        binding.imvZoomOut.setOnClickListener {
            foodViewModel.downAmount()
        }

        binding.tvCheckout.setOnClickListener {
            if (foodViewModel.amount.value!! > 0) {
                val itemInCartEntity = ItemInCartEntity(
                    nameItem = binding.food!!.name,
                    amount = foodViewModel.amount.value!!.toInt(),
                    total = foodViewModel.total.value!!.toInt(),
                    idFood = binding.food!!.id
                )
                cartViewModel.insert(itemInCartEntity)

                cartViewModel.liveItemInCart.value = itemInCartEntity

                cartViewModel.upAmount()
                cartViewModel.listItemInCartLiveData.value?.add(itemInCartEntity)

                foodViewModel.resetAmount()

                findNavController().navigate(R.id.action_oderDetailFragment_to_cartFragment)
            }
        }

        binding.ivBack.setOnClickListener {
            onBackPress()
        }

    }

    override fun initView() {

    }

    override fun onDestroy() {
        super.onDestroy()
        foodViewModel.resetAmount()
    }

}

