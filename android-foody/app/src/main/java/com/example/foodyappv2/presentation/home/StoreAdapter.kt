package com.example.foodyappv2.presentation.home

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.foodyappv2.R
import com.example.foodyappv2.data.model.Store
import com.example.foodyappv2.data.model.StoreInfor
import com.example.foodyappv2.databinding.ItemStoreBinding


class StoreAdapter : RecyclerView.Adapter<StoreAdapter.StoreViewHolder>() {

    var list: MutableList<Store> = mutableListOf()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    var callBack: StoreListener? = null

    class StoreViewHolder(val binding: ItemStoreBinding) :
        RecyclerView.ViewHolder(binding.root) {
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StoreViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = DataBindingUtil.inflate<ItemStoreBinding>(
            layoutInflater,
            R.layout.item_store,
            parent,
            false
        )
        return StoreViewHolder(binding)
    }

    override fun onBindViewHolder(holder: StoreViewHolder, position: Int) {
        val item = list[position]
        holder.binding.item = item
        holder.binding.itemPosition = position
        holder.binding.itemListener = callBack
    }

    override fun getItemCount() = list.size

    interface StoreListener {
        fun onItemClick(index: Int, item: Store)

    }
}