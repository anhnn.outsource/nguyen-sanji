package com.example.foodyappv2.presentation.admin.home.food

import android.annotation.SuppressLint
import android.net.Uri
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.foodyappv2.data.model.Food
import com.example.foodyappv2.databinding.AdminFoodItemBinding

class AdminFoodAdapter : RecyclerView.Adapter<AdminFoodAdapter.FoodVH>() {

    private val dataList = mutableListOf<Food>()
    var listener: AdminFoodListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FoodVH {
        val binding =
            AdminFoodItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return FoodVH(binding)
    }

    override fun onBindViewHolder(holder: FoodVH, position: Int) {
        holder.bindView(dataList[position])
    }

    override fun getItemCount() = dataList.size

    fun setData(categories: List<Food>) {
        this.dataList.apply {
            clear()
            addAll(categories)
            notifyDataSetChanged()
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    fun remove(food: Food) {
        val position = dataList.indexOf(dataList.first {
            food.id.equals(it.id)
        })

        dataList.remove(food)
        notifyDataSetChanged()


    }

    inner class FoodVH(
        private val binding: AdminFoodItemBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        @SuppressLint("SetTextI18n")
        fun bindView(food: Food) {
            binding.apply {
                tvName.text = food.name
                tvPrice.text = "${food.price} VNĐ"

                Glide.with(root.context)
                    .load(food.image)
                    .into(binding.image)

                ivDelete.setOnClickListener {
                    listener?.onDeleteClick(food)
                }

            }
        }
    }

    interface AdminFoodListener {
        fun onDeleteClick(item: Food)
    }
}
