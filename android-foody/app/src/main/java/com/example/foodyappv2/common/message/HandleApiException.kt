package com.example.foodyappv2.common.message

//import com.example.foodyapp.base.extension.getAppString
import android.content.Context
import com.example.foodyappv2.domain.APIException

object HandleApiException : IAPIMessage {

    override fun getAPIMessage(context: Context?, exception: APIException): String? {
        return when (exception.code) {
            APIException.NETWORK_ERROR ->
                    "no network"
//                getAppString(R.string.no_network)
            APIException.TIME_OUT_ERROR ->
                "server time out"
//            getAppString(R.string.sever_time_out)

            else -> {
                if (exception.message.isNullOrBlank()) {
//                    getAppString(R.string.unknown_error)
                    "unknown"
                } else {
                    exception.message
                }
            }
        }
    }
}
