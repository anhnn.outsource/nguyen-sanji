package com.example.foodyappv2.presentation.orderhistory

import android.widget.Toast
import androidx.fragment.app.activityViewModels
import com.example.foodyappv2.R
import com.example.foodyappv2.base.BaseFragment
import com.example.foodyappv2.data.model.Ordered
import com.example.foodyappv2.databinding.FragmentHistoryOrderBinding
import com.example.foodyappv2.utils.observer
import com.example.foodyappv2.utils.onDebouncedClick


class HistoryOrderFragment : BaseFragment<FragmentHistoryOrderBinding>(),
    OrderAdapter.OrderListener {

    private lateinit var orderAdapter: OrderAdapter
    private val viewModel: OrderedViewModel by activityViewModels()

    override fun isCanBackPress() = true

    override fun getLayoutId() = R.layout.fragment_history_order

    override fun initView() {
        viewModel.getData()
        observer(viewModel.ordereds) {
            it?.let {
                orderAdapter.setData(it)
            }
        }

        orderAdapter = OrderAdapter()
        orderAdapter.callBack = this
        binding.rcOrder.adapter = orderAdapter

    }

    override fun setAction() {
        binding.ivBack.onDebouncedClick {
            onBackPress()
        }
    }

    override fun onReceivedOrder(index: Int, item: Ordered) {
        viewModel.updateOrder(
            item,
            onSuccess = {
                viewModel.getData()
            },
            onFail = {
                Toast.makeText(requireActivity(), it, Toast.LENGTH_SHORT).show()
            }, OrderedViewModel.STATE_ORDERED.RECEIVED)
    }

    override fun onCancelOrder(index: Int, item: Ordered) {
        viewModel.updateOrder(
            item,
            onSuccess = {
                viewModel.getData()
            },
            onFail = {
                Toast.makeText(requireActivity(), it, Toast.LENGTH_SHORT).show()
            }, OrderedViewModel.STATE_ORDERED.CANCEL)
    }
}