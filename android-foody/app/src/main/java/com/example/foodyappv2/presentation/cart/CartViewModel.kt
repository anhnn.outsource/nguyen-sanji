package com.example.foodyappv2.presentation.cart

import android.os.Build
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.foodyappv2.AppPreference
import com.example.foodyappv2.R
import com.example.foodyappv2.base.extension.getAppString
import com.example.foodyappv2.data.model.ItemInCart
import com.example.foodyappv2.data.model.Voucher
import com.example.foodyappv2.data.repository.ItemInCartRepository
import com.example.foodyappv2.data.repository.OrderRepository
import com.example.foodyappv2.data.source.local.ItemInCartEntity
import com.example.foodyappv2.data.source.local.OrderEntity
import com.example.foodyappv2.utils.FirebaseUtils
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.time.format.FormatStyle

class CartViewModel : ViewModel() {

    private var firestore = FirebaseUtils.getFireStore()

    private val itemInCartRepository = ItemInCartRepository()
    private val orderRepository = OrderRepository()

    val liveItemInCart = MutableLiveData<ItemInCartEntity>()
    val liveOrders = orderRepository.getLiveDataOrders()
    val listItemInCartLiveData = itemInCartRepository.getAllItemInCartLiveData()

    var amount = MutableLiveData<Int>(0)
    var payment = MutableLiveData<String>("Tiền mặt")
    var isOrdering = MutableLiveData<Boolean>(true)

    var total = MutableLiveData<Int>(0)

    var hasVoucher = MutableLiveData<Boolean>(false)
    var discount = MutableLiveData<Int>(0)
    var nameVoucher = MutableLiveData<String>("")

    fun upAmount() {
        amount.value = liveItemInCart.value?.amount?.let { amount.value?.plus(it) }
        total.value = liveItemInCart.value?.total?.let { total.value?.plus(it) }
    }

    fun downAmount() {
        if (amount.value!! > 0) {
            amount.value = liveItemInCart.value?.amount?.let { amount.value?.minus(it) }
            total.value = liveItemInCart.value?.total?.let { total.value?.minus(it) }
        }
    }

//    @RequiresApi(Build.VERSION_CODES.O)
//    fun checkOut(): Boolean {
//        val currentDateTime = LocalDateTime.now()
//        val time = currentDateTime.format(
//            DateTimeFormatter.ISO_DATE
//        )
//        if (amount.value != 0) {
//            val orderEntity = OrderEntity(
//                name = "Order",
//                total = total.value!! - discount.value!!,
//                paymentMethod = payment.value!!,
//                amount = amount.value!!,
//                date = time,
//                status = true
//            )
//
////            val ordered = Ordered().apply {
////                name = "Order"
////                total = this@CartViewModel.total.value!! - discount.value!!
////                paymentMethod = this@CartViewModel.payment.value!!
////                amount = this@CartViewModel.amount.value
////                date = time
////                status = 0
////                itemInCarts = listItemInCartLiveData.value
////            }
//
//            val ordered = hashMapOf(
//
//                "name" to "Order",
//                "total" to this@CartViewModel.total.value!! - discount.value!!,
//                "paymentMethod" to this@CartViewModel.payment.value!!,
//                "amount" to this@CartViewModel.amount.value,
//                "date" to time,
//                "status" to 0,
//                "itemInCarts" to listItemInCartLiveData.value
//            )
//            firestore.collection("ordered")
//                .add(ordered)
//                .addOnSuccessListener { documentReference ->
//                    onSuccess.invoke(getAppString(R.string.success))
//                }.addOnFailureListener { e ->
//                    e.message?.let { onFailure.invoke(it) }
//                }
//            deleteAll()
//            isOrdering.value = false
//            return true
//        }
//        return false
//    }

    @RequiresApi(Build.VERSION_CODES.O)
    fun checkOut(
        onSuccess: (msg: String) -> Unit,
        onFailure: (msg: String) -> Unit
    ) {
        val currentDateTime = LocalDateTime.now()
        val time = currentDateTime.format(
            DateTimeFormatter.ofLocalizedDateTime(FormatStyle.MEDIUM, FormatStyle.SHORT)
        )
        if (amount.value != 0) {

            var itemInCarts = mutableListOf<ItemInCart>()

            listItemInCartLiveData.value?.forEach {
                itemInCarts.add(ItemInCart().apply {
                    nameItem = it.nameItem
                    amount = it.amount.toLong()
                    total = it.total.toLong()
                    id = it.idFood
                })
            }

            val ordered = hashMapOf(
                "id" to "",
                "name" to "Order",
                "owner" to AppPreference.profile.id,
                "total" to this@CartViewModel.total.value!! - discount.value!!,
                "paymentMethod" to this@CartViewModel.payment.value!!,
                "amount" to this@CartViewModel.amount.value,
                "date" to time,
                "status" to 0,
                "itemInCarts" to itemInCarts,
                "discount" to discount.value
            )
            firestore.collection("ordered")
                .add(ordered)
                .addOnSuccessListener { documentReference ->
                    firestore.collection("ordered").document(documentReference.id)
                        .update("id", documentReference.id)
                        .addOnSuccessListener {
                            onSuccess.invoke(getAppString(R.string.success))
                        }.addOnFailureListener {

                        }
                }.addOnFailureListener { e ->
                    e.message?.let { onFailure.invoke(it) }
                }
            deleteAll()
            isOrdering.value = false
        }
    }

    fun checkoutAgain(orderEntity: OrderEntity) {
        val newOrder = OrderEntity(
            name = "Order", total = orderEntity.total, paymentMethod = orderEntity.paymentMethod,
            amount = orderEntity.amount, date = orderEntity.date, status = true
        )
        insertOrder(newOrder)
    }

    fun insert(itemInCartEntity: ItemInCartEntity) {
        itemInCartRepository.insert(itemInCartEntity)
    }

    fun delete(itemInCartEntity: ItemInCartEntity) {
        itemInCartRepository.delete(itemInCartEntity)

        if (hasVoucher.value!!) {
            hasVoucher.value = false
            discount.value = 0
            nameVoucher.value = ""
        }
    }

    fun insertOrder(orderEntity: OrderEntity) {
        orderRepository.insert(orderEntity)
    }

    fun deleteAll() {
        itemInCartRepository.deleteAll()
        amount.value = 0
        total.value = 0
        hasVoucher.value = false
        discount.value = 0
        nameVoucher.value = ""
    }

    fun isAmountValid(): Boolean {
        return (amount.value!! > 0)
    }

    fun addVoucher(voucher: Voucher): Boolean {

        return if (voucher.disscount > total.value!!) {
            false
        } else {
            hasVoucher.value = true
            nameVoucher.value = voucher.code
            discount.value = voucher.disscount
            true
        }
    }

}