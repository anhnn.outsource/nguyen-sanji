package com.example.foodyappv2.data.repository

import androidx.lifecycle.LiveData
import com.example.foodyappv2.data.source.local.AppDatabase
import com.example.foodyappv2.data.source.local.ItemInCartEntity


class ItemInCartRepository {
    private val database = AppDatabase.getInstance()
    private val itemInCartDao = database.itemInCartDao()

    fun insert(itemInCartEntity: ItemInCartEntity) {
        itemInCartDao.insert(itemInCartEntity)
    }

    fun deleteAll(){
        itemInCartDao.deleteAll()
    }

    fun delete(itemInCartEntity: ItemInCartEntity) {
        itemInCartDao.delete(itemInCartEntity)
    }

    fun getAllItemInCartLiveData(): LiveData<MutableList<ItemInCartEntity>> {
        return itemInCartDao.getAllItemInCartLiveData()
    }

    fun isFoodExist(name:String)=  itemInCartDao.isFoodIsExist(name)
}