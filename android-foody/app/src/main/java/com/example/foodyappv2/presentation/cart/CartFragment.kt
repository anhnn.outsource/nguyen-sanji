package com.example.foodyappv2.presentation.cart

import android.os.Build
import android.os.Handler
import android.view.View
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.example.foodyappv2.R
import com.example.foodyappv2.base.BaseFragment
import com.example.foodyappv2.data.source.local.ItemInCartEntity
import com.example.foodyappv2.databinding.FragmentCartBinding
import com.example.foodyappv2.utils.observer
import com.example.foodyappv2.utils.onDebouncedClick


import java.util.*

class CartFragment : BaseFragment<FragmentCartBinding>(), CartAdapter.CartListener {
    private val TAG = "CartFragment"
    private val cartViewModel: CartViewModel by activityViewModels()
    private var cartAdapter = CartAdapter()
    private var handler = Handler()
    var list = mutableListOf<ItemInCartEntity>()
    var touchHelper: ItemTouchHelper? = null

    override fun getLayoutId() = R.layout.fragment_cart

    override fun isCanBackPress() = true

    override fun initBinding() {
        binding.viewModel = cartViewModel
    }

    override fun initView() {

        // load list item tronggior hàng
        cartViewModel.listItemInCartLiveData.observe(viewLifecycleOwner) {
            it?.let { list ->
                cartAdapter.list = list
            }
        }

        // đăng ký quan sát biến số lượng đồ ăn khi nó thay đổi
        observer(cartViewModel.amount){
            cartViewModel.isAmountValid()
        }

        cartAdapter.callBack = this
        binding.rcItemInCart.adapter = cartAdapter

        // đổi chỗ item trong recyclerview
        touchHelper =
            ItemTouchHelper(object :
                ItemTouchHelper.SimpleCallback(ItemTouchHelper.UP or ItemTouchHelper.DOWN, 0) {
                override fun onMove(
                    p0: RecyclerView,
                    p1: RecyclerView.ViewHolder,
                    p2: RecyclerView.ViewHolder
                ): Boolean {
                    val sourcePosition = p1.adapterPosition
                    val targetPosition = p2.adapterPosition
                    Collections.swap(cartAdapter.list, sourcePosition, targetPosition)
                    binding.rcItemInCart.adapter?.notifyItemMoved(sourcePosition, targetPosition)
                    return true
                }

                override fun onSwiped(p0: RecyclerView.ViewHolder, p1: Int) {

                }

            })

        touchHelper?.attachToRecyclerView(binding.rcItemInCart)
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun setAction() {
        binding.ivBack.onDebouncedClick {
            onBackPress()
        }

        binding.tvOrder.onDebouncedClick {
            cartViewModel.checkOut(
                onSuccess = {
                    findNavController().popBackStack(R.id.homeFragment, false)
                },
                onFailure = {
                    binding.tvNotifyAdd.visibility = View.VISIBLE
                    handler.postDelayed(Runnable {
                        binding.tvNotifyAdd.visibility = View.GONE
                    }, 1500)
                }
            )
        }
        binding.tvAddPromo.onDebouncedClick {
            findNavController().navigate(R.id.action_cartFragment_to_addPromoFragment)
        }
        binding.tvDirect.onDebouncedClick {

        }
        binding.tvAddDetailAddress.onDebouncedClick {

        }
        binding.tvEdit.onDebouncedClick {

        }
    }

    override fun onItemClick(index: Int, item: ItemInCartEntity) {

    }

    // xóa item trong giỏ hàng
    override fun onDeleteItem(item: ItemInCartEntity) {
        val builder = AlertDialog.Builder(requireContext())
        builder.setTitle("FoodyApp")
        builder.setMessage("Remove ${item.nameItem.toString()} ?")
            .setCancelable(false)
            .setPositiveButton("Yes") { dialog, id ->
                cartViewModel.delete(item)
                cartViewModel.liveItemInCart.value = item
                cartViewModel.downAmount()
            }
            .setNegativeButton("No") { dialog, id ->
                dialog.cancel()
            }
        val alert = builder.create()
        alert.show()
    }
}