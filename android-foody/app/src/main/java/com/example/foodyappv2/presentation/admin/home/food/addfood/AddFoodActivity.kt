package com.example.foodyappv2.presentation.admin.home.food.addfood

import ai.ftech.dev.base.common.ActivityConfig
import ai.ftech.dev.base.common.BaseActivity
import ai.ftech.dev.base.common.bindingadapter.onSafeClick
import android.content.ActivityNotFoundException
import android.content.Intent
import android.graphics.drawable.BitmapDrawable
import android.net.Uri
import android.provider.MediaStore
import android.util.Log
import android.widget.Toast
import com.example.foodyappv2.R
import com.example.foodyappv2.base.extension.getAppString
import com.example.foodyappv2.data.event.AddFoodEvent
import com.example.foodyappv2.databinding.AddFoodActivityBinding
import com.google.android.gms.tasks.OnFailureListener
import com.google.android.gms.tasks.OnSuccessListener
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import org.greenrobot.eventbus.EventBus
import java.io.IOException
import kotlin.random.Random


class AddFoodActivity : BaseActivity<AddFoodActivityBinding>() {
    private val TAG = "AddFoodActivity"
    private val imageNumber =
        generateSequence { Random.nextInt(1000) }.distinct().take(4).toSet()
    private val imageUrl = "image${imageNumber}.jpg"
    private val REQUEST_IMAGE_CODE = 1
    private lateinit var filePath: Uri
    private lateinit var storage: StorageReference
    private lateinit var firebaseStorage: FirebaseStorage
    private val db = FirebaseFirestore.getInstance()

    override fun getConfig(): ActivityConfig {
        return object : ActivityConfig {
            override fun getLayoutResource(): Int {
                return R.layout.add_food_activity
            }
        }
    }

    override fun onInitView() {
        // khởi tạo firebase
        firebaseStorage = FirebaseStorage.getInstance()
        storage = firebaseStorage.reference

        with(binding) {
            //back màn hình
            ivBack.onSafeClick {
                finish()
            }

            //  gọi hàm checkout
            tvComplete.onSafeClick {
                if (checkInput()) {

                    addFood()

                    // bắn 1 thông báo là DB vừa được thêm

                } else {
                    // hiển thị thông báo thất bại
                    Toast.makeText(
                        this@AddFoodActivity,
                        getAppString(R.string.failure),
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }

            cvFoodImage.onSafeClick {
                dispatchTakePictureIntent()
            }
        }
    }

    // 1 - check dữ liệu được nhập
    private fun checkInput(): Boolean {
        return (binding.edtName.text.toString() != ""
                && binding.edtIdStore.text.toString() != ""
                && binding.edtPrice.text.toString() != "")
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_IMAGE_CODE && resultCode == RESULT_OK) {
            if (!(data != null && data.data != null)) {
                return
            }
            filePath = data.data!!
            try {
                val bitmap = MediaStore.Images.Media.getBitmap(contentResolver, filePath)
                val ob = BitmapDrawable(resources, bitmap)
                binding.ivFoodImage.setImageDrawable(ob)
                uploadImage()
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
    }


    // 2 - tạo đồ ăn trên firebase
    private fun addFood() {
        try {
            val food = hashMapOf(
                "id" to "",
                "name" to binding.edtName.text.toString(),
                "price" to binding.edtPrice.text.toString(),
                "idStore" to binding.edtIdStore.text.toString()
            )
            db.collection("foods")
                .add(food)
                .addOnSuccessListener { documentReference ->

                    db.collection("foods").document(documentReference.id)
                        .update("id", documentReference.id)
                        .addOnSuccessListener {

                        }.addOnFailureListener {

                        }

                    val ref = imageUrl.let {
                        Log.d(TAG, "addFood: $it")

                        storage.child(it)

                    }
                    ref.downloadUrl
                        .addOnSuccessListener(OnSuccessListener<Uri?> {

                            Log.d(TAG, "addFood: ${it}")
                            db.collection("foods").document(documentReference.id)
                                .update("image", it).addOnSuccessListener {
                                    EventBus.getDefault().post(AddFoodEvent())
                                    finish()
                                }
                        })
                }.addOnFailureListener { e ->
                    Toast.makeText(this, e.message, Toast.LENGTH_SHORT).show()
                }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }


    private fun dispatchTakePictureIntent() {
        val intent = Intent()
        intent.type = "image/*"
        intent.action = Intent.ACTION_GET_CONTENT
        try {
            startActivityForResult(
                Intent.createChooser(intent, "Select Picture"),
                REQUEST_IMAGE_CODE
            )
        } catch (e: ActivityNotFoundException) {

        }
    }

    private fun uploadImage() {
        val ref =
            storage.child(imageUrl)
        ref.putFile(filePath)
    }
}
