package com.example.foodyappv2.presentation.admin.home

import ai.ftech.dev.base.common.ActivityConfig
import ai.ftech.dev.base.common.BaseActivity
import ai.ftech.dev.base.common.bindingadapter.onSafeClick
import android.Manifest
import android.content.Intent
import android.content.Intent.FLAG_GRANT_READ_URI_PERMISSION
import android.content.pm.PackageManager
import android.os.Build
import android.os.Messenger
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.example.foodyappv2.R
import com.example.foodyappv2.databinding.AdminHomeActivityBinding
import com.example.foodyappv2.presentation.admin.home.food.AdminFoodFragment
import com.example.foodyappv2.presentation.admin.home.ordered.AdminOrderFragment
import com.example.foodyappv2.presentation.admin.home.store.AdminStoreFragment


class AdminHomeActivity : BaseActivity<AdminHomeActivityBinding>() {

    val adapter = StaffStateManager(supportFragmentManager)

    override fun getConfig(): ActivityConfig {
        return object : ActivityConfig {
            override fun getLayoutResource(): Int {
                return R.layout.admin_home_activity
            }
        }
    }

    override fun onInitView() {

        val tabList = listOf(AdminFoodFragment(), AdminStoreFragment(), AdminOrderFragment())
        val titleList = listOf("Foods", "Stores", "Order")
        with(binding) {

            adapter.addTab(tabList)
            adapter.addTabTitle(titleList)


            vpAdminHome.adapter = adapter
            tlAdminHome.setupWithViewPager(vpAdminHome)

            ivAdminHomeLogOut.onSafeClick {
                finish()
            }
        }
    }

}