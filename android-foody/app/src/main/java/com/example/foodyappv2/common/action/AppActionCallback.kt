package com.example.foodyappv2.common.action

import com.example.foodyappv2.base.BaseAction

open class AppActionCallback<RESPONSE> : BaseAction.IActionCallback<RESPONSE> {

    override fun onSuccess(response: RESPONSE?) {
    }

    override fun onException(throwable: Throwable?) {
        throwable?.printStackTrace()
    }
}
