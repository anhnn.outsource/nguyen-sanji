package com.example.foodyappv2.presentation.profile

import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.navigation.fragment.findNavController
import com.example.foodyappv2.AppPreference
import com.example.foodyappv2.R
import com.example.foodyappv2.base.BaseFragment
import com.example.foodyappv2.databinding.FragmentProfileBinding
import com.example.foodyappv2.utils.onDebouncedClick

import com.google.firebase.auth.FirebaseAuth

class ProfileFragment : BaseFragment<FragmentProfileBinding>() {


    private var firebaseAuth = FirebaseAuth.getInstance()
    override fun isCanBackPress() = true

    override fun getLayoutId(): Int {
        return R.layout.fragment_profile
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {

        }
    }

    override fun initView() {
        if (!AppPreference.profile.name.isNullOrEmpty()) {
            binding.tvProfile.text = AppPreference.profile.name
        } else{
            binding.tvProfile.text = "Please create name"
        }
    }

    override fun setAction() {
        binding.ivBack.onDebouncedClick {
            onBackPress()
        }

        binding.tvProfile.onDebouncedClick {
            findNavController().navigate(R.id.action_profileFragment_to_editProfileFragment)
        }

        binding.tvMyOrder.onDebouncedClick {
            findNavController().navigate(R.id.action_profileFragment_to_historyOrderFragment)
        }

        binding.tvInviteFriend.onDebouncedClick {
            findNavController().navigate(R.id.action_profileFragment_to_inviteFriendFragment)
        }

        binding.tvRule.onDebouncedClick {
            findNavController().navigate(R.id.action_profileFragment_to_fragmentPolicy)
        }

        binding.tvCenterHelps.onDebouncedClick {
            findNavController().navigate(R.id.action_profileFragment_to_helpCenterFragment)

        }
        binding.tvSetting.setOnClickListener { }

        binding.tvLanguage.setOnClickListener { }

        binding.tvLogOut.onDebouncedClick {
            val builder = AlertDialog.Builder(requireContext())
            builder.setTitle("Foody")
            builder.setMessage("Do you want to log out?")
                .setCancelable(false)
                .setPositiveButton("Yes") { dialog, id ->
                    firebaseAuth.signOut()
                    findNavController().popBackStack(R.id.loginFragment, false)
                }
                .setNegativeButton("No") { dialog, id ->
                    dialog.cancel()
                }
            val alert = builder.create()
            alert.show()
        }
    }


}