package com.example.foodyappv2.presentation.cart.addpromo

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.foodyappv2.R
import com.example.foodyappv2.data.model.Voucher
import com.example.foodyappv2.databinding.ItemCouponBinding

class CouponAdapter : RecyclerView.Adapter<CouponAdapter.CouponViewHolder>() {

    var list: MutableList<Voucher> = mutableListOf()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    var callBack: CouponListener? = null

    class CouponViewHolder(val binding: ItemCouponBinding) : RecyclerView.ViewHolder(binding.root) {

    }

    interface CouponListener {
        fun onItemClick(index: Int, item: Voucher)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CouponViewHolder {

        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = DataBindingUtil.inflate<ItemCouponBinding>(
            layoutInflater,
            R.layout.item_coupon,
            parent,
            false
        )
        return CouponViewHolder(binding)
    }

    override fun onBindViewHolder(holder: CouponViewHolder, position: Int) {

        val item = list[position]

        holder.binding.item = item
        holder.binding.listener = callBack
        holder.binding.position = position
    }

    override fun getItemCount() = list.size
}