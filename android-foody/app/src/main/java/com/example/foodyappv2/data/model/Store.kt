package com.example.foodyappv2.data.model

import com.example.foodyappv2.data.source.local.ItemInCartEntity

class Store(
    var id: String,
    var name: String,
    var address: String,
    var rate: String,
    var image: MutableList<String>,
    var category: String,
    var range: String = " "

)