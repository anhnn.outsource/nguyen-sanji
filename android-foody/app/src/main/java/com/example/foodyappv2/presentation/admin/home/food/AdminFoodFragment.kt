package com.example.foodyappv2.presentation.admin.home.food

import ai.ftech.dev.base.common.FragmentConfig
import ai.ftech.dev.base.common.bindingadapter.onSafeClick
import android.content.Intent
import android.util.Log
import android.widget.Toast
import com.example.foodyappv2.R
import com.example.foodyappv2.base.BaseFragment
import com.example.foodyappv2.base.extension.getAppString
import com.example.foodyappv2.common.FoodyFragment
import com.example.foodyappv2.data.event.AddFoodEvent
import com.example.foodyappv2.data.model.Food
import com.example.foodyappv2.databinding.AdminFoodFragmentBinding
import com.example.foodyappv2.presentation.admin.home.food.addfood.AddFoodActivity
import com.example.foodyappv2.presentation.admin.home.store.addstore.AddStoreActivity
import com.google.firebase.firestore.FirebaseFirestore
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe

class AdminFoodFragment : BaseFragment<AdminFoodFragmentBinding>() {
    private var firestore: FirebaseFirestore = FirebaseFirestore.getInstance()

    val adapter = AdminFoodAdapter().apply {
        listener = object : AdminFoodAdapter.AdminFoodListener {
            override fun onDeleteClick(item: Food) {
                deleteFood(item)
            }

        }
    }

    override fun getLayoutId(): Int {
        return R.layout.admin_food_fragment
    }

    override fun initView() {
        EventBus.getDefault().register(this);
        getData()

        with(binding) {

            rvAdminFoods.adapter = adapter
            cvAdminFoodAdd.onSafeClick {
                startActivity(Intent(requireActivity(), AddFoodActivity::class.java))
            }
        }
    }

    override fun setAction() {
    }

    // 1 - lấy dữ liệu từ DB
    private fun getData() {
        val productRefs = firestore.collection("foods")
        val foods = mutableListOf<Food>()
        productRefs
            .get().addOnSuccessListener {
                it?.let { it ->
                    it.forEach {
                        foods.add(
                            Food(
                                it.get("id").toString(),
                                it.get("name").toString(),
                                it.get("price").toString(),
                                it.get("image").toString(),
                                it.get("idStore").toString()
                            )
                        )
                    }
                }

                adapter.setData(foods)
            }.addOnFailureListener {

            }
    }

    // xóa đồ ăn
    private fun deleteFood(food: Food) {
        firestore.collection("foods").document(food.id)
            .delete()
            .addOnSuccessListener {
                Toast.makeText(requireContext(), getAppString(R.string.success), Toast.LENGTH_SHORT).show()
                adapter.remove(food)
            }
            .addOnFailureListener { e ->
                Toast.makeText(requireContext(), e.message, Toast.LENGTH_SHORT).show()
            }
    }

    override fun onDestroy() {
        super.onDestroy()
        EventBus.getDefault().unregister(this);
    }

    @Subscribe
    fun onEvent(event: AddFoodEvent) {
        getData()
    }
}