package com.example.foodyappv2.presentation.orderhistory

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.foodyappv2.R
import com.example.foodyappv2.base.extension.getAppColor
import com.example.foodyappv2.base.extension.gone
import com.example.foodyappv2.base.extension.show
import com.example.foodyappv2.data.model.Ordered
import com.example.foodyappv2.data.source.local.ItemInCartEntity
import com.example.foodyappv2.databinding.ItemOrderBinding
import com.example.foodyappv2.presentation.cart.CartAdapter
import com.example.foodyappv2.utils.onDebouncedClick

class OrderAdapter : RecyclerView.Adapter<OrderAdapter.OrderViewHolder>() {

    private var list: MutableList<Ordered> = mutableListOf()

    var callBack: OrderListener? = null

    @SuppressLint("NotifyDataSetChanged")
    fun setData(data: List<Ordered>) {
        list.clear()
        list.addAll(data)
        notifyDataSetChanged()
    }

    override fun getItemViewType(position: Int): Int {
        return super.getItemViewType(position)
    }

    inner class OrderViewHolder(val binding: ItemOrderBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun onBind(index: Int, item: Ordered) {

            val adapter = CartAdapter()
            var itemsInCarts = mutableListOf<ItemInCartEntity>()
            item.itemInCarts?.forEach {
                var item = ItemInCartEntity(
                    idFood = it.id,
                    amount = it.amount?.toInt() ?: 0,
                    nameItem = it.nameItem.toString(),
                    total = it.total?.toInt() ?: 0
                )

                itemsInCarts.add(item)
            }

            adapter.list = itemsInCarts
            binding.rvItemInCart.adapter = adapter
            adapter.notifyDataSetChanged()

            binding.tvTotal.text = item.total.toString() + " $"
            binding.tvDate.text = item.date.toString()
            binding.tvStatus.text = item.getStatus()
            binding.tvDetail.text = item.paymentMethod
            binding.tvVoucher.text = "Discount: ${item.discount.toString()} $"

            when (item.status) {
                0L -> {
                    binding.ivStatus.setImageResource(R.drawable.ic_processing)
                    binding.tvStatus.setTextColor(getAppColor(R.color.dark_yellow))
                    binding.reOrder.show()
                    binding.tvCancelOrder.show()
                }
                1L -> {
                    binding.ivStatus.setImageResource(R.drawable.ic_check)
                    binding.tvStatus.setTextColor(getAppColor(R.color.light_blue))
                    binding.reOrder.gone()
                    binding.tvCancelOrder.gone()

                }
                else -> {
                    binding.ivStatus.setImageResource(R.drawable.ic_warn)
                    binding.tvStatus.setTextColor(getAppColor(R.color.red))
                    binding.reOrder.gone()
                    binding.tvCancelOrder.gone()

                }
            }

            binding.reOrder.onDebouncedClick {
                callBack?.onReceivedOrder(index, item)
            }
            binding.tvCancelOrder.onDebouncedClick {
                callBack?.onCancelOrder(index, item)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OrderViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = DataBindingUtil.inflate<ItemOrderBinding>(
            layoutInflater,
            R.layout.item_order,
            parent,
            false
        )
        return OrderViewHolder(binding)
    }

    override fun onBindViewHolder(holder: OrderViewHolder, position: Int) {
        val item = list[position]

        holder.onBind(position, item)

    }

    override fun getItemCount() = list.size

    interface OrderListener {
        fun onReceivedOrder(index: Int, item: Ordered)
        fun onCancelOrder(index: Int, item: Ordered)
    }
}