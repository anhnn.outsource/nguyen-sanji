package com.example.foodyappv2.common.recyclerview

import android.content.Context
import android.graphics.Canvas
import android.graphics.Rect
import android.graphics.drawable.Drawable
import android.view.View
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ItemDecoration

open class DividerDecorator : ItemDecoration {
    var drawable: Drawable? = null
        private set
    private var mLeft = 0
    private var mTop = 0
    private var mRight = 0
    private var mBottom = 0
    private val mDividerOutRect = Rect()
    private var mShowLast = false

    constructor() {}
    constructor(context: Context?, resId: Int) {
        drawable = ContextCompat.getDrawable(context!!, resId)
        if (drawable != null) {
            drawable!!.setBounds(0, 0, drawable!!.intrinsicWidth, drawable!!.intrinsicHeight)
        }
    }

    constructor(drawable: Drawable?) {
        this.drawable = drawable
    }

    constructor(drawable: Drawable?, showLast: Boolean) {
        this.drawable = drawable
        mShowLast = showLast
    }

    constructor(context: Context?, resId: Int, showLast: Boolean) {
        drawable = ContextCompat.getDrawable(context!!, resId)
        mShowLast = showLast
    }

    fun setPadding(left: Int, top: Int, right: Int, bottom: Int) {
        mLeft = left
        mTop = top
        mRight = right
        mBottom = bottom
    }

    override fun getItemOffsets(
        outRect: Rect,
        view: View,
        parent: RecyclerView,
        state: RecyclerView.State
    ) {
        super.getItemOffsets(outRect, view, parent, state)
        if (parent.getChildAdapterPosition(view) == parent.adapter!!.itemCount - 1) {
            return
        }
        outRect.bottom = drawable!!.intrinsicHeight
    }

    override fun onDraw(c: Canvas, parent: RecyclerView, state: RecyclerView.State) {
        val dividerLeft = parent.paddingLeft + mLeft
        val dividerRight = parent.width - parent.paddingRight - mRight
        val count = parent.childCount
        for (i in 0 until count) {
            if (i == count - 1 && !mShowLast) {
                return
            }
            val child = parent.getChildAt(i)
            getDividerOffset(mDividerOutRect, child, parent, state)
            if (mDividerOutRect.height() == 0) {
                continue
            } else {
                drawable!!.setBounds(
                    mDividerOutRect.left,
                    mDividerOutRect.top,
                    mDividerOutRect.right,
                    mDividerOutRect.bottom
                )
                drawable!!.draw(c)
            }
            /*RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) child.getLayoutParams();
            int dividerTop = child.getBottom() + params.bottomMargin + mTop;
            int dividerBottom = dividerTop + mDivider.getIntrinsicHeight() - mBottom;
            mDivider.setBounds(dividerLeft, dividerTop, dividerRight, dividerBottom);
            mDivider.draw(c);*/
        }
    }

    /**
     * Determine bound of divider. This bound decide the position of divider
     *
     * @param rect   bound of divider
     * @param view
     * @param parent
     * @param state
     */
    open fun getDividerOffset(
        rect: Rect, view: View, parent: RecyclerView,
        state: RecyclerView.State
    ) {
        val params = view.layoutParams as RecyclerView.LayoutParams
        val dividerLeft = parent.paddingLeft + mLeft
        val dividerTop = view.bottom + params.bottomMargin + mTop
        val dividerRight = parent.width - parent.paddingRight - mRight
        val dividerBottom = dividerTop + drawable!!.intrinsicHeight - mBottom
        rect[dividerLeft, dividerTop, dividerRight] = dividerBottom
    }
}