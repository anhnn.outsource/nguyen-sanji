package com.example.foodyappv2.presentation.storedetail

import android.os.Bundle
import android.view.View
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.LinearSnapHelper
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.example.foodyappv2.R
import com.example.foodyappv2.base.BaseFragment
import com.example.foodyappv2.databinding.FragmentImageStoreDetailBinding
import com.example.foodyappv2.presentation.shareviewmodel.FoodViewModel
import com.google.common.base.MoreObjects


class ImageStoreDetailFragment : BaseFragment<FragmentImageStoreDetailBinding>(),
    StoreImageAdapter.FoodImageListener {

    private val detailViewModel: FoodViewModel by activityViewModels()
    private lateinit var foodImageAdapter: StoreImageAdapter
    private var foodImageList = mutableListOf<Int>()

    override fun isCanBackPress() = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {

        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

    }

    override fun setAction() {
        binding.ivBack.setOnClickListener {
            onBackPress()
        }

    }

    override fun initView() {
        val imageFoodLayoutManager = StaggeredGridLayoutManager(1, RecyclerView.HORIZONTAL)
        val snapHelper = LinearSnapHelper()
        snapHelper.attachToRecyclerView(binding.rcFoodImage)

        detailViewModel.liveBigFood.observe(viewLifecycleOwner) {
            foodImageAdapter = StoreImageAdapter()
            foodImageAdapter.callBack = this
            foodImageAdapter.list = it.image

            binding.rcFoodImage.layoutManager = imageFoodLayoutManager
            binding.rcFoodImage.adapter = foodImageAdapter
        }

    }

    override fun getLayoutId() = R.layout.fragment_image_store_detail

    override fun onItemClick(index: Int, item: String) {

    }

}