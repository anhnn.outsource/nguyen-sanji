package com.example.foodyappv2.data.repository

import androidx.lifecycle.LiveData
import com.example.foodyappv2.data.source.local.AppDatabase
import com.example.foodyappv2.data.source.local.SearchEntity


class SearchRepository {

    private val database = AppDatabase.getInstance()
    private val searchDao = database.searchDao()

    fun insert(searchEntity: SearchEntity) {
        searchDao.insert(searchEntity)
    }

    fun getRecentSearch(): List<SearchEntity> {
        return searchDao.getRecentSearch()
    }

    fun getRecentSearchLiveData(): LiveData<List<SearchEntity>> {
        return searchDao.getRecentSearchLiveData()
    }

}