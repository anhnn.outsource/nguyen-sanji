package com.example.foodyappv2.common.recyclerview

import ai.ftech.dev.base.adapter.group.GroupAdapter
import ai.ftech.dev.base.adapter.group.GroupData
import android.content.Context
import android.util.AttributeSet
import androidx.annotation.IntRange
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.foodyappv2.common.model.ISelectionItem

class GridGroupRecyclerView(context: Context, attrs: AttributeSet?) : RecyclerView(context, attrs) {

    private var maxItemHorizontal: Int = 2

    init {
        this.layoutManager = getGridLayoutManager()
    }

    /**
     * Need to call before notifyDataSetChange
     */
    fun setMaxItemHorizontal(@IntRange(from = 1, to = 10) number: Int) {
        maxItemHorizontal = number
        this.layoutManager = getGridLayoutManager()
    }

    fun setAdapter(groupAdapter: GroupAdapter) {
        this.adapter = groupAdapter
    }

    private fun getGridLayoutManager(): LayoutManager {
        val spanCount = getFactorialNumber(maxItemHorizontal)
        return GridLayoutManager(context, spanCount, GridLayoutManager.VERTICAL, false).apply {
            this.spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
                override fun getSpanSize(position: Int): Int {
                    val groupData =
                        (adapter as GroupAdapter).groupManager.findGroupDataByAdapterPosition(
                            position
                        )
                    return if (groupData is GridGroupData<*>) {
                        groupData.getItemSpanSize(position, spanCount)
                    } else {
                        spanCount
                    }
                }
            }
        }
    }

    private fun getFactorialNumber(number: Int): Int {
        var factorial = 1
        if (number <= 0) {
            return 0
        }
        for (i in 1..number) {
            factorial *= number
        }
        return factorial
    }

    abstract class GridGroupData<T>(itemList: List<T>) :
        GroupData<MutableList<T>>(itemList.toMutableList()) {


        open fun getPositionInItemList(position: Int): Int = -1

        abstract fun getGroupPositionFromPositionInItemList(positionInItemList: Int): Int

//        abstract fun getDecoration(): RecyclerView.ItemDecoration?

        open fun getMaxItemHorizontalByViewType(viewType: Int): Int {
            return 1
        }

        fun getItemSpanSize(adapterPosition: Int, spanCount: Int): Int {
            val positionInGroup = mapAdapterPositionToGroupPosition(adapterPosition)
            val viewType = getItemViewType(positionInGroup)
            return spanCount / getMaxItemHorizontalByViewType(viewType)
        }
    }

    abstract class SelectGridGroupData<DATA>(itemList: List<DATA>) :
        GridGroupData<ISelectionItem<DATA>>(mutableListOf()) {

        var maxItemSelected = 1

        fun select(item: DATA?) {
            val newIndex = data.indexOfFirst { it.itemIsTheSame(item) }

            if (newIndex != -1) {
                val newItem = data[newIndex]
                if (!newItem.isSelected) {
                    when (maxItemSelected) {
                        1 -> {
                            val oldIndex = data.indexOfFirst { it.isSelected }
                            if (oldIndex != -1) {
                                val oldItem = data[oldIndex]
                                oldItem.isSelected = false

                                notifyChanged(getGroupPositionFromPositionInItemList(oldIndex))
                            }
                            newItem.isSelected = true
                        }
                        else -> {
                            newItem.isSelected = true
                        }
                    }
                } else {
                    when (maxItemSelected) {
                        1 -> {
                            newItem.isSelected = (getMinItemSelected() > 0)
                        }
                        else -> {
                            newItem.isSelected = false
                        }
                    }
                }
                notifyChanged(getGroupPositionFromPositionInItemList(newIndex))
            }
        }

        fun selectAll() {
            data.forEach {
                it.isSelected = true
            }
            notifyChanged(getGroupPositionFromPositionInItemList(0), data.size)
        }

        fun clearAll() {
            data.forEach {
                it.isSelected = false
            }
            notifyChanged(getGroupPositionFromPositionInItemList(0), data.size)
        }

        open fun getMinItemSelected(): Int = 1

        open fun resetDataList(newDataList: List<DATA>) {
            val dataList = newDataList.mapIndexed { index, data ->
                getDataSelectionItem(data, index)
            }.toMutableList()

            data = dataList
            notifyChanged()
        }

        open fun getDataSelectionItem(item: DATA, position: Int): ISelectionItem<DATA> {
            return object : ISelectionItem<DATA> {
                override var isSelected = false
                override fun getData(): DATA = item
                override fun itemIsTheSame(data: DATA?) = false
                override fun match(keySearch: String): Boolean = false
            }
        }
    }
}