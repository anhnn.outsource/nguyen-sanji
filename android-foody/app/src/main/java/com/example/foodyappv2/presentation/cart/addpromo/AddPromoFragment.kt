package com.example.foodyappv2.presentation.cart.addpromo

import android.widget.Toast
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.example.foodyappv2.R
import com.example.foodyappv2.base.BaseFragment
import com.example.foodyappv2.data.model.Voucher
import com.example.foodyappv2.databinding.FragmentAddPromoBinding
import com.example.foodyappv2.presentation.cart.CartViewModel
import com.example.foodyappv2.utils.DataFake
import com.example.foodyappv2.utils.onDebouncedClick

class AddPromoFragment : BaseFragment<FragmentAddPromoBinding>() {

    private var couponAdapter = CouponAdapter()
    private val cartViewMode: CartViewModel by activityViewModels()

    override fun getLayoutId() = R.layout.fragment_add_promo

    override fun isCanBackPress() = true

    override fun initView() {
        val layoutManagerCoupon = StaggeredGridLayoutManager(1, RecyclerView.VERTICAL)

        couponAdapter.list = DataFake.getVoucherData()
        couponAdapter.callBack = object : CouponAdapter.CouponListener {
            override fun onItemClick(index: Int, item: Voucher) {
                if (cartViewMode.addVoucher(item)) {
                    onBackPress()
                } else {
                    Toast.makeText(requireContext(), "Add item to use Voucher!",Toast.LENGTH_SHORT).show()
                }
            }
        }
        binding.rclvCoupons.layoutManager = layoutManagerCoupon
        binding.rclvCoupons.adapter = couponAdapter


    }

    override fun setAction() {
        binding.ivBack.onDebouncedClick {
            onBackPress()
        }
    }
}