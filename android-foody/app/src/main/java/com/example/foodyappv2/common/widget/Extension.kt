package com.example.foodyappv2.common.widget

import com.example.foodyappv2.base.extension.getApplication
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import com.example.foodyappv2.common.message.HandleApiException
import com.example.foodyappv2.domain.APIException

fun <T> Flow<T>.onException(onCatch: (Throwable) -> Unit): Flow<T> {
    return catch { e ->
        if (e is APIException) {
            val apiException = APIException(
                HandleApiException.getAPIMessage(
                    getApplication(),
                    e
                ), e.code)
            onCatch(apiException)
        } else {
            onCatch(e)
        }
    }
}
