package com.example.foodyappv2.data.source.remote.network.config

import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response
import com.example.foodyappv2.data.source.remote.network.ApiConfig
import java.io.IOException

open class FoodyRetrofitConfig() : BaseRetrofitConfig() {

    override fun getUrl() = ApiConfig.BASE_URL_FOODY

    override fun getInterceptors(): Array<Interceptor>? {
        return arrayOf(ContentTypeUrlencoded())
    }

    open class ContentTypeUrlencoded() : Interceptor {

        @Throws(IOException::class)
        override fun intercept(chain: Interceptor.Chain): Response {
            val requestBuilder = buildChain(chain)
            return chain.proceed(requestBuilder.build())
        }

        open fun buildChain(chain: Interceptor.Chain): Request.Builder {
            val original = chain.request()
            val builder = original.newBuilder()
            builder.addHeader("Content-Type", "application/x-www-form-urlencoded")
            builder.method(original.method, original.body)
            return builder
        }
    }
}
