package com.example.foodyappv2.presentation.admin.home.store

import ai.ftech.dev.base.common.bindingadapter.onSafeClick
import android.content.Intent
import android.util.Log
import android.widget.Toast
import com.example.foodyappv2.R
import com.example.foodyappv2.base.BaseFragment
import com.example.foodyappv2.base.extension.getAppString
import com.example.foodyappv2.data.event.AddFoodEvent
import com.example.foodyappv2.data.event.AddStoreEvent
import com.example.foodyappv2.data.model.Food
import com.example.foodyappv2.data.model.Store
import com.example.foodyappv2.databinding.AdminStoreFragmentBinding
import com.example.foodyappv2.presentation.admin.home.store.addstore.AddStoreActivity
import com.google.firebase.firestore.FirebaseFirestore
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe

class AdminStoreFragment : BaseFragment<AdminStoreFragmentBinding>() {
    var firestore: FirebaseFirestore = FirebaseFirestore.getInstance()

    val adapter = AdminStoreAdapter().apply {
        listener = object : AdminStoreAdapter.AdminStoreListener {
            override fun onDeleteClick(item: Store) {
                deleteFood(item)
            }

        }
    }

    override fun getLayoutId(): Int {
        return R.layout.admin_store_fragment
    }

    override fun initView() {
        EventBus.getDefault().register(this);
        with(binding) {
            rvAdminStores.adapter = adapter

            cvAdminStoreAdd.onSafeClick {
                startActivity(Intent(requireActivity(), AddStoreActivity::class.java))
            }
        }
        getData()
    }

    override fun setAction() {
    }

    private fun getData() {
        val productRefs = firestore.collection("stores")
        val stores = mutableListOf<Store>()
        productRefs
            .get().addOnSuccessListener {
                it?.let { it ->
                    it.forEach {
                        stores.add(
                            Store(
                                it.get("id").toString(),
                                it.get("name").toString(),
                                it.get("address").toString(),
                                it.get("rate").toString(),
                                mutableListOf(it.get("image").toString()),
                                it.get("category").toString()
                            )
                        )
                    }
                }
                adapter.setData(stores)
            }.addOnFailureListener {

            }
    }


    private fun deleteFood(store: Store) {
        firestore.collection("stores").document(store.id)
            .delete()
            .addOnSuccessListener {
                Toast.makeText(requireContext(), getAppString(R.string.success), Toast.LENGTH_SHORT).show()
                adapter.remove(store)
            }
            .addOnFailureListener { e ->
                Toast.makeText(requireContext(), e.message, Toast.LENGTH_SHORT).show()
            }
    }

    override fun onDestroy() {
        super.onDestroy()
        EventBus.getDefault().unregister(this);
    }

    @Subscribe
    fun onEvent(event: AddStoreEvent) {
        getData()
    }
}