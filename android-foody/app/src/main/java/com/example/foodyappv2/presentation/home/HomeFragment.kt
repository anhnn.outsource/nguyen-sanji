package com.example.foodyappv2.presentation.home

import android.os.Handler
import android.util.Log
import android.view.View
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearSnapHelper
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.example.foodyappv2.AppPreference
import com.example.foodyappv2.R
import com.example.foodyappv2.base.BaseFragment
import com.example.foodyappv2.databinding.FragmentHomeBinding
import com.example.foodyappv2.data.model.*
import com.example.foodyappv2.presentation.shareviewmodel.FoodViewModel
import com.example.foodyappv2.presentation.cart.CartViewModel
import com.example.foodyappv2.presentation.voucher.VoucherAdapter
import com.example.foodyappv2.utils.DataFake
import com.example.foodyappv2.utils.observer

class HomeFragment : BaseFragment<FragmentHomeBinding>(), FoodAdapterHor.FoodListener,
    StoreAdapter.StoreListener, VoucherAdapter.VoucherListener {

    private var touchHelper: ItemTouchHelper? = null
    private val TAG = "HomeFragment"
    private lateinit var foodAdapter: FoodAdapterHor
    private lateinit var voucherAdapter: VoucherAdapter
    private lateinit var bigAdapter: StoreAdapter
    private var voucherList = mutableListOf<Voucher>()
    private var bigFoodList = mutableListOf<Store>()
    private val detailViewModel: FoodViewModel by activityViewModels()
    private val cartViewModel: CartViewModel by activityViewModels()
    private var handler = Handler()


    override fun getLayoutId(): Int {
        return R.layout.fragment_home
    }

    override fun initBinding() {
        binding.cartViewModel = cartViewModel
    }

    override fun setAction() {
        binding.view.setOnClickListener {

        }
        binding.ivNotify.setOnClickListener {
        }

        binding.tvUsername.setOnClickListener {
        }

        binding.ivCart.setOnClickListener {
            findNavController().navigate(R.id.action_homeFragment_to_cartFragment)
        }

        binding.tvTitle1.setOnClickListener {

        }

        binding.ivMenu.setOnClickListener {
            findNavController().navigate(R.id.action_homeFragment_to_profileFragment)
        }

        binding.tvSearch.setOnClickListener {
            findNavController().navigate(R.id.action_homeFragment_to_searchFragment)
        }

        binding.tvMore.setOnClickListener {
            findNavController().navigate(R.id.action_homeFragment_to_allFoodFragment)
        }
    }

    override fun initView() {
        observer(cartViewModel.isOrdering) {
            if (!it!!) {
                binding.tvCompleteOrder.visibility = View.VISIBLE
                handler.postDelayed(Runnable {
                    binding.tvCompleteOrder.visibility = View.GONE
                }, 2500)
                cartViewModel.isOrdering.value = true
            }
        }

        AppPreference.profile.avatarBitmap?.let {
            binding.rivUser.setImageBitmap(it)
        }

        binding.tvUsername.text = "Hello ${AppPreference.profile.name}"

        handler.postDelayed(Runnable {
            binding.view.visibility = View.GONE
        }, 1000)

        detailViewModel.getAllFood()
        detailViewModel.getAllStore()
        detailViewModel.foodsLiveData.observe(viewLifecycleOwner) {
            Log.d(TAG, "initView: ${it.size}")
            foodAdapter = FoodAdapterHor()
            foodAdapter.list = detailViewModel.foodsLiveData.value!!
            foodAdapter.callBack = this
            binding.popularRecyclerview.adapter = foodAdapter
        }

        detailViewModel.storesLiveData.observe(viewLifecycleOwner){
            bigAdapter.list = it
            bigAdapter.notifyDataSetChanged()
        }

        voucherList = DataFake.getVoucherData()

        val layoutManagerVoucher = StaggeredGridLayoutManager(1, RecyclerView.HORIZONTAL)
        val layoutManagerBigFood = StaggeredGridLayoutManager(1, RecyclerView.HORIZONTAL)
        val snapHelper = LinearSnapHelper()

        snapHelper.attachToRecyclerView(binding.slideRecycler)

        voucherAdapter = VoucherAdapter()
        voucherAdapter.callBack = this
        voucherAdapter.list = voucherList

        bigAdapter = StoreAdapter()
        bigAdapter.callBack = this
        bigAdapter.list = bigFoodList

        binding.rvVoucher.layoutManager = layoutManagerVoucher
        binding.rvVoucher.adapter = voucherAdapter

        binding.slideRecycler.layoutManager = layoutManagerBigFood
        binding.slideRecycler.adapter = bigAdapter

        touchHelper?.attachToRecyclerView(binding.popularRecyclerview)
    }

    override fun onItemClick(index: Int, item: Food) {
        detailViewModel.liveFood.value = (item)
        detailViewModel.amount.value = 1
        detailViewModel.total.value = item.getPriceToInt()
        findNavController().navigate(R.id.action_homeFragment_to_oderDetailFragment)
    }

    override fun onItemClick(index: Int, item: Store) {
        detailViewModel.liveBigFood.value = (item)
        findNavController().navigate(R.id.action_homeFragment_to_bigFoodDetailFragment)
    }

    override fun onItemClick(index: Int, item: Voucher) {
        detailViewModel.liveVoucher.value = item
        findNavController().navigate(R.id.action_homeFragment_to_voucherFragment)
    }
}
