package com.example.foodyappv2.presentation.admin.login

import ai.ftech.dev.base.common.ActivityConfig
import ai.ftech.dev.base.common.BaseActivity
import ai.ftech.dev.base.common.bindingadapter.onSafeClick
import android.content.Intent
import android.widget.Toast
import com.example.foodyappv2.R
import com.example.foodyappv2.base.extension.getAppString
import com.example.foodyappv2.data.model.Food
import com.example.foodyappv2.databinding.AdminLoginActivityBinding
import com.example.foodyappv2.presentation.admin.home.AdminHomeActivity
import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.FirebaseFirestore

class AdminLoginActivity : BaseActivity<AdminLoginActivityBinding>() {
    var firestore: FirebaseFirestore = FirebaseFirestore.getInstance()


    override fun getConfig(): ActivityConfig {
        return object : ActivityConfig {
            override fun getLayoutResource(): Int {
                return R.layout.admin_login_activity
            }
        }
    }

    override fun onInitView() {
        binding.tvEnter.onSafeClick {

            if (binding.edtAdminCode.text.toString().isNotEmpty()) {
                val code = binding.edtAdminCode.text.toString()
                val productRefs: CollectionReference = firestore.collection("AdminCode")
                productRefs
                    .get().addOnSuccessListener {
                        it?.let {
                            it.forEach {
                                if (it.getString("code").equals(code)) {
                                    startActivity(Intent(this, AdminHomeActivity::class.java))
                                }
                            }
                        }
                    }.addOnFailureListener {
                        Toast.makeText(
                            this,
                            it.message,
                            Toast.LENGTH_SHORT
                        ).show()
                    }
            }
        }

    }

}