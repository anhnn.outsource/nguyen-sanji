package com.example.foodyappv2.utils

import android.annotation.SuppressLint
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore

object FirebaseUtils {

    @SuppressLint("StaticFieldLeak")
    private var firestore: FirebaseFirestore? = null
    private var firebaseAuth: FirebaseAuth? = null

    fun fireStoreInit() {
        firestore = FirebaseFirestore.getInstance()
        firebaseAuth = FirebaseAuth.getInstance()
    }

    fun getFireStore(): FirebaseFirestore {
        return firestore ?: FirebaseFirestore.getInstance()
    }

    fun getFirebaseAuth(): FirebaseAuth {
        return firebaseAuth ?: FirebaseAuth.getInstance()
    }
}
