package com.example.foodyappv2.data.source.remote.network.config

import com.example.foodyappv2.BuildConfig
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

abstract class BaseRetrofitConfig {

    abstract fun getUrl(): String
    abstract fun getInterceptors(): Array<Interceptor>?

    fun getRetrofit(): Retrofit {
        return getRetrofitBuilder().build()
    }

    fun getRetrofitBuilder(): Retrofit.Builder {
        return Retrofit.Builder()
            .baseUrl(getUrl())
//            .addConverterFactory(ScalarsConverterFactory.create())
            .addConverterFactory(GsonConverterFactory.create(getGson()))
            .client(provideOkHttpClient())
//            .addConverterFactory(NullOnEmptyConverterFactory())
    }

    private fun provideOkHttpClient(): OkHttpClient {
        val logging = HttpLoggingInterceptor()
        if (BuildConfig.DEBUG) {
            logging.level = HttpLoggingInterceptor.Level.BODY
        } else {
            logging.level = HttpLoggingInterceptor.Level.NONE
        }

        val builder = OkHttpClient.Builder().addInterceptor(logging)
        val interceptors = getInterceptors()

        interceptors?.let {
            it.forEach { interceptor ->
                builder.addInterceptor(interceptor)
            }
        }
        builder.connectTimeout(30, TimeUnit.SECONDS)
            .readTimeout(30, TimeUnit.SECONDS)
            .writeTimeout(30, TimeUnit.SECONDS)
            .retryOnConnectionFailure(false)
//            .sslSocketFactory(SSLSocketClient.getSSLSocketFactory(), object : X509TrustManager {
//                override fun checkClientTrusted(p0: Array<out X509Certificate>?, p1: String?) {
//
//                }
//
//                override fun checkServerTrusted(p0: Array<out X509Certificate>?, p1: String?) {
//                }
//
//                override fun getAcceptedIssuers(): Array<X509Certificate> {
//                    return arrayOf()
//                }
//            })

        return builder.build()
    }

    private fun getGson(): Gson {
        return GsonBuilder()
            .setLenient()
            .excludeFieldsWithoutExposeAnnotation()
            .setFieldNamingPolicy(com.google.gson.FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
            .create()
//            .registerTypeAdapter(java.util.Date::class.java, GsonUtcDateAdapter())
    }
}
