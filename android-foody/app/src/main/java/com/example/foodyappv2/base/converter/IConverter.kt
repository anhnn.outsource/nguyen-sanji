package com.example.foodyappv2.base.converter

internal interface IConverter<S, D> {
    fun convert(source: S): D
}
