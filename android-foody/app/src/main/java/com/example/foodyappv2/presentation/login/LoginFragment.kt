package com.example.foodyappv2.presentation.login

import ai.ftech.dev.base.common.FragmentConfig
import ai.ftech.dev.base.common.bindingadapter.onSafeClick
import android.content.Intent
import android.text.Editable
import android.text.TextWatcher
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.example.foodyappv2.R
import com.example.foodyappv2.common.FoodyFragment
import com.example.foodyappv2.common.widget.headeralert.HEADER_ALERT_TYPE
import com.example.foodyappv2.data.model.USER_ROLE
import com.example.foodyappv2.databinding.FragmentLoginBinding
import com.example.foodyappv2.presentation.admin.home.AdminHomeActivity
import com.example.foodyappv2.presentation.admin.login.AdminLoginActivity
import com.example.foodyappv2.utils.onDebouncedClick

class LoginFragment : FoodyFragment<FragmentLoginBinding>() {

    private val TAG = "LoginFragment"
    private val loginViewModel: LoginViewModel by viewModels()

    override fun getConfig(): FragmentConfig {
        return object : FragmentConfig {
            override fun getLayoutResource() = R.layout.fragment_login
        }
    }

    override fun onInitView() {

        binding.edtMail.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if (!emailValid()) {
                    binding.edtMail.error = "Malformed"
                }
            }

            override fun afterTextChanged(p0: Editable?) {

            }

        })

        binding.edtPass.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if (!passValid()) {
                    binding.edtPass.error = "Pass more than 6 letter"
                }
            }

            override fun afterTextChanged(p0: Editable?) {
            }
        })
    }

    override fun onViewClick() {
        binding.layoutRegister.setOnClickListener {
            findNavController().navigate(R.id.action_loginFragment_to_registerFragment)
        }

        binding.tvLogin.onDebouncedClick {
            if (checkInput()) login()
        }
    }

    private fun checkInput(): Boolean {
        return emailValid() && passValid()
    }

    private fun login() {
        showLoading()
        val mail = binding.edtMail.text.toString()
        val pass = binding.edtPass.text.toString()

        loginViewModel.login(mail, pass, onSuccess = { msg, role ->
            hideLoading()
            showHeaderAlert(msg, HEADER_ALERT_TYPE.SUCCESS)

            when (role) {
                USER_ROLE.ADMIN -> startActivity(Intent(
                    requireActivity(), AdminHomeActivity::class.java)
                )
                else -> findNavController().navigate(R.id.action_loginFragment_to_homeFragment)
            }
        }, onFailure = {
            hideLoading()
            showHeaderAlert(it, HEADER_ALERT_TYPE.ERROR)
        })
    }

    fun emailValid(): Boolean {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(binding.edtMail.text)
            .matches() && binding.edtMail.text.isNotEmpty()
    }

    fun passValid(): Boolean {
        return (binding.edtPass.text.length > 5) && binding.edtPass.text.isNotEmpty()

    }

}
