package com.example.foodyappv2.data.source.remote

import com.example.foodyappv2.data.modelserver.DataResponse
import com.example.foodyappv2.data.source.remote.base.IApiService
import retrofit2.Call
import retrofit2.http.GET

interface FoodyService :IApiService{
    @GET("/food")
    fun getFoodList() : Call<DataResponse>
}