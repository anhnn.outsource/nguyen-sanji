package com.example.foodyappv2.data.repository

import com.example.foodyappv2.data.source.local.AppDatabase
import com.example.foodyappv2.data.source.local.OrderEntity


class OrderRepository {
    private val database = AppDatabase.getInstance()
    private val orderDao = database.orderDao()

    fun insert(orderEntity: OrderEntity) {
        orderDao.insert(orderEntity)
    }

    fun getLiveDataOrders() = orderDao.getAllLiveDataOrders()

}