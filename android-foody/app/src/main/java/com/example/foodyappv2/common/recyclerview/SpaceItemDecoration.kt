package com.example.foodyappv2.common.recyclerview

import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ItemDecoration

class SpaceItemDecoration(
    private val mSpace: Int,
    private val mOrientation: Int = LinearLayoutManager.VERTICAL
) :
    ItemDecoration() {
    override fun getItemOffsets(
        outRect: Rect, view: View, parent: RecyclerView,
        state: RecyclerView.State
    ) {
        if (mOrientation == LinearLayoutManager.VERTICAL) {
            if (parent.getChildAdapterPosition(view) != 0) {
                outRect.top = mSpace
            }
        } else if (mOrientation == LinearLayoutManager.HORIZONTAL) {
            if (parent.getChildAdapterPosition(view) != 0) {
                outRect.left = mSpace
            }
        }
    }
}