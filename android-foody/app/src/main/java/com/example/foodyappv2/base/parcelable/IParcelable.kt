package com.example.foodyappv2.base.parcelable

import android.os.Parcelable

internal interface IParcelable: Parcelable, Cloneable {

    override fun describeContents(): Int {
        return 0
    }

    override fun clone(): Any {
        return super.clone()
    }
}