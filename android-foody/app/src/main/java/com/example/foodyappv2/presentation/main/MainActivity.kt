package com.example.foodyappv2.presentation.main

import ai.ftech.dev.base.common.ActivityConfig
import android.os.Bundle
import android.view.WindowManager
import androidx.activity.viewModels
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import com.example.foodyappv2.R
import com.example.foodyappv2.common.FoodyActivity
import com.example.foodyappv2.databinding.ActivityMainBinding
import com.example.foodyappv2.presentation.cart.CartViewModel

class MainActivity : FoodyActivity<ActivityMainBinding>() {

    private lateinit var navController: NavController
    private val cartViewModel : CartViewModel by viewModels()

    override fun getConfig(): ActivityConfig {
        return object :ActivityConfig{
            override fun getLayoutResource() = R.layout.activity_main

        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )

        val navHostFragment = supportFragmentManager.findFragmentById(
            R.id.nav_host_fragment
        ) as NavHostFragment
        navController = navHostFragment.navController
    }

    override fun onStop() {
        super.onStop()
        cartViewModel.deleteAll()
    }
}