package com.example.foodyappv2.presentation.admin.home.store

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.foodyappv2.data.model.Food
import com.example.foodyappv2.data.model.Store
import com.example.foodyappv2.data.model.StoreInfor
import com.example.foodyappv2.databinding.AdminFoodItemBinding
import com.example.foodyappv2.databinding.AdminStoreItemBinding

class AdminStoreAdapter : RecyclerView.Adapter<AdminStoreAdapter.StoreVH>() {

    private val dataList = mutableListOf<Store>()
    var listener: AdminStoreListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StoreVH {
        val binding =
            AdminStoreItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return StoreVH(binding)
    }

    override fun onBindViewHolder(holder: StoreVH, position: Int) {
        holder.bindView(dataList[position])
    }

    override fun getItemCount() = dataList.size

    fun setData(categories: List<Store>) {
        this.dataList.apply {
            clear()
            addAll(categories)
            notifyDataSetChanged()
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    fun remove(store: Store) {
        val position = dataList.indexOf(dataList.first {
            store.id.equals(it.id)
        })

        dataList.remove(store)
        notifyDataSetChanged()
    }

    inner class StoreVH(
        private val binding: AdminStoreItemBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun bindView(store: Store) {
            binding.apply {
                tvName.text = store.name
                tvAddress.text = store.address
                tvCategory.text = store.category
                tvRate.text = store.rate

                Glide.with(root.context)
                    .load(store.image[0])
                    .into(binding.image)

                ivDelete.setOnClickListener {
                    listener?.onDeleteClick(store)
                }
            }
        }
    }

    interface AdminStoreListener {
        fun onDeleteClick(item: Store)
    }
}