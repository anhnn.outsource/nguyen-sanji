package com.example.foodyappv2.common

import com.example.foodyappv2.domain.APIException
import com.example.foodyappv2.utils.getApplication
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import com.example.foodyappv2.common.message.HandleApiException

fun <T> Flow<T>.onException(onCatch: (Throwable) -> Unit): Flow<T> {
    return catch { e ->
        if (e is APIException) {
            val apiException = APIException(
                HandleApiException.getAPIMessage(
                    getApplication(),
                    e
                ), e.code)
            onCatch(apiException)
        } else {
            onCatch(e)
        }
    }
}
