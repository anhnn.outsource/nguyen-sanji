package com.example.foodyappv2.domain.action

import com.example.foodyappv2.AppPreference
import com.example.foodyappv2.base.BaseAction
import com.google.firebase.auth.FirebaseAuth
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

internal class LoginAction : BaseAction<LoginAction.LoginRV, Boolean>() {

    private var firebaseAuth = FirebaseAuth.getInstance()

    override suspend fun execute(requestValue: LoginRV): Boolean {
        var result = false

        return withContext(dispatcher) {
            launch {
                firebaseAuth.signInWithEmailAndPassword(requestValue.email, requestValue.pass)
                    .addOnCompleteListener {
                        if (it.isSuccessful) {
                            AppPreference.profile.apply {
                                email = requestValue.email
                            }
                            result = true
                        } else {
                            result = false
                        }
                    }
                    .addOnFailureListener {
                        result = false
                    }
            }
            result
        }
    }

    internal class LoginRV(var email: String, var pass: String) : RequestValue


}