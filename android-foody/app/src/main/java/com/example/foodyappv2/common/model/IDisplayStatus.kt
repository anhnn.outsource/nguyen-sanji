package com.example.foodyappv2.common.model

import android.graphics.drawable.Drawable

interface IDisplayStatus<DATA> {
    fun getData(): DATA
    fun getTitle(): String = ""
    fun getIcon(): Drawable {
        throw IllegalArgumentException("Can not find icon for status")
    }

    fun getColor(): Int {
        throw IllegalArgumentException("Can not find color for status")
    }
}
